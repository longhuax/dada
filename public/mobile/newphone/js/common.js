/*$(window).load(function() {
  $("img.lazy").unveil(10, function() {
    $(this).load(function() {
      this.style.opacity = 1;
    });
  });
});*/

// 通用消息提示框
function showAlert(content) {
  layer.open({
    skin: 'layer-alert',
    content: content,
    btn: ['OK']
  });
}

function showMessage(content) {
    if(layer && layer.msg) {
      layer.msg(content);
    } else {
      layer.open({
        content
        ,skin: 'msg'
        ,time: 2 //2秒后自动关闭
      });
    }
}

// 背景透明渐变
function bgcOpacity() {
  function opacityInner() {
    var scrollTop_val = $(window).scrollTop() / 150;

    if ( scrollTop_val > 1 ) {
      items.css({opacity: 1});
    } else {
      items.css({opacity: scrollTop_val});
    }
  }
  $(window).scroll( function() {
    opacityInner();
  });
}

$(document).ready(function() {
  $("body.product-category .nav-bar #more-category").on('click', function () {
    $('.side-menu').addClass('active');
    $('body').addClass('body-overflow');
  });

    var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if(is_safari) {
        $(document).on('touchstart',function (e) {
            var target = $(e.target);
            if (target.closest("body.product-category .nav-bar #more-category, .side-menu.active").length == 0) {
                $('.side-menu').removeClass('active');
                $('body').removeClass('body-overflow');
            }
        });
    } else {
        $(document).on('click', function (e) {
            var target = $(e.target);
            if (target.closest("body.product-category .nav-bar #more-category, .side-menu.active").length == 0) {
                $('.side-menu').removeClass('active');
                $('body').removeClass('body-overflow');
            }
        });
    }


  // 购物车删除弹出
  $('.checkout-cart .edit').click(function(event) {
    $(this).toggleClass('active');
    $('.product-item .cart-quantity-wrapper').toggleClass('edit-remove');
  });

  jQuery(window).scroll(function(){
    if (jQuery(this).scrollTop() > 100) {
      jQuery('.go-top').fadeIn();
      jQuery('.go-home').fadeIn();
    } else {
      jQuery('.go-top').fadeOut();
      jQuery('.go-home').fadeOut();
    }
  });
  // scroll-to-top animate
  jQuery('.go-top').click(function(){
    jQuery("html, body").animate({ scrollTop: 0 }, 400);
      return false;
  });

  // Append * to required form input
  $('.form-group').each(function () {
    if ($(this).hasClass('required')) {
      $(this).find('input').attr('placeholder', function(i, val) {
        return val + '*';
      });
    }
  });
});

/* *
 * 截取小数位数
 */
function advFormatNumber(value, num) // 四舍五入
{
    var a_str = formatNumber(value, num);
    var a_int = parseFloat(a_str);
    if (value.toString().length > a_str.length) {
        var b_str = value.toString().substring(a_str.length, a_str.length + 1);
        var b_int = parseFloat(b_str);
        if (b_int < 5) {
            return a_str;
        } else {
            var bonus_str, bonus_int;
            if (num == 0) {
                bonus_int = 1;
            } else {
                bonus_str = "0."
                for (var i = 1; i < num; i++)
                    bonus_str += "0";
                bonus_str += "1";
                bonus_int = parseFloat(bonus_str);
            }
            a_str = formatNumber(a_int + bonus_int, num)
        }
    }
    return a_str;
}

function formatNumber(value, num) // 直接去尾
{
    var a, b, c, i;
    a = value.toString();
    b = a.indexOf('.');
    c = a.length;
    if (num == 0) {
        if (b != -1) {
            a = a.substring(0, b);
        }
    } else {
        if (b == -1) {
            a = a + ".";
            for (i = 1; i <= num; i++) {
                a = a + "0";
            }
        } else {
            a = a.substring(0, b + num + 1);
            for (i = c; i <= b + num; i++) {
                a = a + "0";
            }
        }
    }
    return a;
}
//加法函数
function accAdd(arg1, arg2) {
    var r1, r2, m;
    try {
        r1 = arg1.toString().split(".")[1].length;
    }
    catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    }
    catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2));

    return (arg1 * m + arg2 * m) / m;
    //return advFormatNumber((arg1 * m + arg2 * m) / m, 2);
}

//减法函数
function Subtr(arg1, arg2) {
    var r1, r2, m, n;
    try {
        r1 = arg1.toString().split(".")[1].length;
    }
    catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    }
    catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2));
    //last modify by deeka
    //动态控制精度长度
    n = (r1 >= r2) ? r1 : r2;
    return ((arg1 * m - arg2 * m) / m).toFixed(n);
    //return advFormatNumber(((arg1 * m - arg2 * m) / m).toFixed(n), 2);
}


//乘法函数
function accMul(arg1, arg2) {
    var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
    try {
        m += s1.split(".")[1].length;
    }
    catch (e) {
    }
    try {
        m += s2.split(".")[1].length;
    }
    catch (e) {
    }
    return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
    //return advFormatNumber(Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m), 2);
}

//除法函数
function accDiv(arg1, arg2) {
    var t1 = 0, t2 = 0, r1, r2;
    try {
        t1 = arg1.toString().split(".")[1].length;
    }
    catch (e) {
    }
    try {
        t2 = arg2.toString().split(".")[1].length;
    }
    catch (e) {
    }
    with (Math) {
        r1 = Number(arg1.toString().replace(".", ""));
        r2 = Number(arg2.toString().replace(".", ""));
        return (r1 / r2) * pow(10, t2 - t1);
        //return advFormatNumber((r1 / r2) * pow(10, t2 - t1), 2);
    }
}

