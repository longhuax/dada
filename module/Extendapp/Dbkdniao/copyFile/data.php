<?php
return array(
    'express' => array(
        0 => array(
            'express_name' => '顺丰速运',
            'express_code' => 'SF',
        ),
        1 => array(
            'express_name' => '百世快递',
            'express_code' => 'HTKY',
        ),
        2 => array(
            'express_name' => '中通快递',
            'express_code' => 'ZTO',
        ),
        3 => array(
            'express_name' => '申通快递',
            'express_code' => 'STO',
        ),
        4 => array(
            'express_name' => '圆通速递',
            'express_code' => 'YTO',
        ),
        5 => array(
            'express_name' => '韵达速递',
            'express_code' => 'YD',
        ),
        6 => array(
            'express_name' => '邮政快递',
            'express_code' => 'YZPY',
        ),
        7 => array(
            'express_name' => 'EMS',
            'express_code' => 'EMS',
        ),
        8 => array(
            'express_name' => '天天快递',
            'express_code' => 'HHTT',
        ),
        9 => array(
            'express_name' => '京东物流',
            'express_code' => 'JD',
        ),
        10 => array(
            'express_name' => '全峰快递',
            'express_code' => 'QFKD',
        ),
        11 => array(
            'express_name' => '国通快递',
            'express_code' => 'GTO',
        ),
        12 => array(
            'express_name' => '优速快递',
            'express_code' => 'UC',
        ),
        13 => array(
            'express_name' => '德邦',
            'express_code' => 'DBL',
        ),
        14 => array(
            'express_name' => '快捷快递',
            'express_code' => 'FASY',
        ),
        15 => array(
            'express_name' => '宅急送',
            'express_code' => 'ZJS',
        ),
        16 => array(
            'express_name' => '安捷快递',
            'express_code' => 'AJ',
        ),
        17 => array(
            'express_name' => '如风达',
            'express_code' => 'RFD',
        ),
        18 => array(
            'express_name' => '中铁快运',
            'express_code' => 'ZTKY',
        ),
        19 => array(
            'express_name' => 'DHL',
            'express_code' => 'DHL',
        ),
        20 => array(
            'express_name' => 'UPS',
            'express_code' => 'UPS',
        ),
    ),
);
