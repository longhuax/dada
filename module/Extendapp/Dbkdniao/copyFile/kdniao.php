<?php

class kdniaoExpressApi {
    /**
     * 公共调用方法
     * @param $expressApi
     * @param $expressNameCode
     * @param $expressNumber
     * @param $httpType
     * @param string $apiType
     * @return array
     */
    public function stateContent($expressApi, $expressNameCode, $expressNumber, $httpType)
    {
        $array = array();
        $array = $this->kdniaoStateContent($expressApi, $expressNameCode, $expressNumber, $httpType);

        return $array;
    }
    /**
     * 快递鸟接口
     * @param $expressApi
     * @param $expressNameCode
     * @param $expressNumber
     * @param $httpType
     * @return array
     */
    private function kdniaoStateContent($expressApi, $expressNameCode, $expressNumber, $httpType){
        $api_url = "http://api.kdniao.com/Ebusiness/EbusinessOrderHandle.aspx";
        $expressNumber = trim($expressNumber);
        $expressNameCode = trim($expressNameCode);
        $requestData = "{'OrderCode':'','ShipperCode':'{$expressNameCode}','LogisticCode':'{$expressNumber}'}";
        $datas = array(
            'EBusinessID' => $expressApi['api_secret'],
            'RequestType' => '1002',
            'RequestData' => urlencode($requestData),
            'DataType' => '2',
        );
        $datas['DataSign'] = base64_encode(md5($requestData.$expressApi['api_code']));
        $post_data = http_build_query($datas);
        $ch = curl_init();
        $header[] = 'Content-Type:application/x-www-form-urlencoded;charset=utf-8';
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        $result = curl_exec($ch);
        $data = json_decode($result, true);
        //返回结果
        $array = array();
        $array['api_type'] = 'kdniao';
        if(isset($data['Success']) && $data['Success'] === false && !empty($data['Reason'])){
            $array['content'] = $data['Reason'];
            return $array;
        }
        if(!isset($data['Traces']) || !is_array($data['Traces'])){
            $array['content'] = '暂时查不到有效物流轨迹';
            return $array;
        }
        $data['Traces'] = array_reverse($data['Traces']);
        //输出界面
        $content = '<table style="width: 98%; margin-left: 5px;">';
        $content .= '<tr><td width="27%" style="padding-left: 8px;background: #64AADB;border: #75C2EF 1px solid;color: #FFFFFF;font-weight: bold;">时间</td>';
        $content .= '<td width="73%" style="padding-left: 8px;background: #64AADB;border: #75C2EF 1px solid;color: #FFFFFF;font-weight: bold;">地点和跟踪进度</td></tr>';
        foreach ($data['Traces'] as $dKey => $value) {
            $content .= '<tr>';
            $content .= '<td style="padding-left: 8px;padding-bottom: 5px;padding-top: 5px;border: 1px solid #DDDDDD;'.($dKey == 0 ? 'color:#FF6600;' : '').'">';
            $content .= $value['AcceptTime'];
            $content .= '</td>';
            $content .= '<td style="padding-left: 8px;padding-bottom: 5px;padding-top: 5px;border: 1px solid #DDDDDD;'.($dKey == 0 ? 'color:#FF6600;' : '').'">';
            $content .= $value['AcceptStation'];
            $content .= '</td></tr>';
        }
        $content .= '</table>';
        $array['content'] = $content;
        return $array;
    }
}