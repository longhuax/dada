��             +         �     �     �     �     �     �     �  !     -   '     U     b     o     |     �  c   �     �                    "     &  !   9     [     n     �     �     �  '   �  -   �  6        >     E  �  L          !     (     5     B     I  !   V  -   x     �     �     �     �     �  c   �     K     X     _     l     s     w  !   �     �     �     �     �     �  '   	  -   *	  6   X	     �	     �	     	                   
                                                                                                                        上架 下架 修改价格 入库数量 删除 商品价格 商品价格与库存批量修改 商品价格与库存，批量修改成功！ 商品名称 商品属性 商品类型 商品规格 商品货号 在输入框中输入商品部分或者全部名称，然后点击添加按钮，即可添加商品 实物商品 尺寸 当前库存 操作 无 查看帮助说明 点击添加将要修改的商品 点击清空全部 点击进行批量修改 状态 虚拟商品 该商品不存在！ 该商品不存在，请重新选择！ 该商品已经存在，无需重复选择！ 请输入需要修改的商品部分或者全部名称 返回 颜色 Project-Id-Version: DBShop V1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-05-31 17:41+0800
PO-Revision-Date: 2018-05-31 17:43+0800
Last-Translator: Baron <baron@loongdom.cn>
Language-Team: DBShop <team@dbshop.net>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 2.0.7
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 上架 下架 修改價格 入庫數量 刪除 商品價格 商品價格與庫存批量修改 商品價格與庫存，批量修改成功！ 商品名稱 商品屬性 商品類型 商品規格 商品貨號 在輸入框中輸入商品部分或者全部名稱，然後點擊添加按鈕，即可添加商品 實物商品 尺寸 當前庫存 操作 無 查看幫助說明 點擊添加將要修改的商品 點擊清空全部 點擊進行批量修改 狀態 虛擬商品 該商品不存在！ 該商品不存在，請重新選擇！ 該商品已經存在，無需重複選擇！ 請輸入需要修改的商品部分或者全部名稱 返回 顏色 