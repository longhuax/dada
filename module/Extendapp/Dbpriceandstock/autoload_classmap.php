<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */
return array(
    'Dbpriceandstock\Module'                      => __DIR__ . '/Module.php',
    'Dbpriceandstock\Controller\IndexController'  => __DIR__ . '/src/Dbpriceandstock/Controller/IndexController.php',

);
