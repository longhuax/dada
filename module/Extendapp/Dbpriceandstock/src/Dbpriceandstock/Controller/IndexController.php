<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */
namespace Dbpriceandstock\Controller;

use Admin\Controller\BaseController;
use Zend\View\Model\ViewModel;

class IndexController extends BaseController
{
    public function indexAction()
    {
        $array = array();

        if($this->request->isPost()) {
            $goodsArray  = $this->request->getPost()->toArray();
            if(isset($goodsArray['goods_id']) and is_array($goodsArray['goods_id']) and !empty($goodsArray['goods_id'])) {
                foreach ($goodsArray['goods_id'] as $idValue) {
                    $goodsInfo  = $this->getDbshopTable('GoodsTable')->oneGoodsInfo(array('goods_id'=>$idValue));
                    if(!empty($goodsInfo)) {
                        $goodsPrice = floatval(trim($goodsArray['goods_price'][$idValue]));
                        $goodsStock = intval(trim($goodsArray['goods_stock'][$idValue]));

                        //价格修改
                        if($goodsPrice > 0) {
                            $this->getDbshopTable('GoodsTable')->oneUpdateGoods(array('goods_shop_price'=>$goodsPrice), array('goods_id'=>$idValue));
                        }
                        //规格价格修改
                        if(isset($goodsArray['extend_goods_price'][$idValue]) and is_array($goodsArray['extend_goods_price'][$idValue]) and !empty($goodsArray['extend_goods_price'][$idValue])) {
                            foreach ($goodsArray['extend_goods_price'][$idValue] as $extendPriceKey => $extendPriceValue) {
                                $extendPriceValue = intval(trim($extendPriceValue));
                                if($extendPriceValue > 0) {
                                    $where = array();
                                    $where['goods_id']      = $idValue;
                                    if($goodsInfo->goods_spec_type == 2) {
                                        $where['adv_spec_tag_id'] = $extendPriceKey;
                                        if(empty($where['adv_spec_tag_id'])) continue;
                                    } else {
                                        $colorAndSizeArray= explode('|', $extendPriceKey);
                                        $where['goods_color']   = $colorAndSizeArray[0];
                                        $where['goods_size']    = $colorAndSizeArray[1];

                                        if(empty($where['goods_color']) or empty($where['goods_size'])) continue;
                                    }
                                    $this->getDbshopTable('GoodsPriceExtendGoodsTable')->updatePriceExtendGoods(array('goods_extend_price'=>$extendPriceValue), $where);
                                }
                            }
                        }

                        //库存修改
                        $goodsStockNum = str_replace(array('+', '-'), array('', ''), $goodsStock);
                        if($goodsStockNum != 0) {
                            if(stripos($goodsStock, '-') !== false) {
                                $goodsStockNum = ($goodsStockNum <= $goodsInfo->goods_stock ? ($goodsInfo->goods_stock - $goodsStockNum) : 0);
                            } else {
                                $goodsStockNum = $goodsStockNum + $goodsInfo->goods_stock;
                            }
                            $this->getDbshopTable('GoodsTable')->oneUpdateGoods(array('goods_stock'=>$goodsStockNum), array('goods_id'=>$idValue));
                        }
                        //规格库存修改
                        if(isset($goodsArray['extend_goods_stock'][$idValue]) and is_array($goodsArray['extend_goods_stock'][$idValue]) and !empty($goodsArray['extend_goods_stock'][$idValue])) {
                            $extendPriceGoodsArray = $this->getDbshopTable('GoodsPriceExtendGoodsTable')->listPriceExtendGoods(array('goods_id'=>$idValue));
                            $pGArray = array();
                            foreach ($extendPriceGoodsArray as $pGValue) {
                                if($goodsInfo->goods_spec_type == 2) {
                                    $pGArray[$pGValue['adv_spec_tag_id']] = $pGValue['goods_extend_stock'];
                                } else {
                                    $pGArray[$pGValue['goods_color'].'|'.$pGValue['goods_size']] = $pGValue['goods_extend_stock'];
                                }
                            }

                            foreach ($goodsArray['extend_goods_stock'][$idValue] as $extendStockKey => $extendStockValue) {
                                $extendStockValue       = intval(trim($extendStockValue));
                                $extendGoodsStockNum    = str_replace(array('+', '-'), array('', ''), $extendStockValue);
                                if($extendGoodsStockNum != 0 and $pGArray[$extendStockKey]) {
                                    if(stripos($extendStockValue, '-') !== false) {
                                        $extendGoodsStockNum = ($extendGoodsStockNum <= $pGArray[$extendStockKey] ? ($pGArray[$extendStockKey] - $extendGoodsStockNum) : 0);
                                    } else {
                                        $extendGoodsStockNum = $extendGoodsStockNum + $pGArray[$extendStockKey];
                                    }

                                    $extendWhere = array();
                                    $extendWhere['goods_id'] = $idValue;
                                    if($goodsInfo->goods_spec_type == 2) {
                                        $extendWhere['adv_spec_tag_id'] = $extendStockKey;
                                        if(empty($extendWhere['adv_spec_tag_id'])) continue;
                                    } else {
                                        $colorAndSizeArray= explode('|', $extendStockKey);
                                        $extendWhere['goods_color']   = $colorAndSizeArray[0];
                                        $extendWhere['goods_size']    = $colorAndSizeArray[1];

                                        if(empty($extendWhere['goods_color']) or empty($extendWhere['goods_size'])) continue;
                                    }
                                    $this->getDbshopTable('GoodsPriceExtendGoodsTable')->updatePriceExtendGoods(array('goods_extend_stock'=>$extendGoodsStockNum), $extendWhere);
                                }
                            }
                        }
                    }
                }
            }
            //跳转处理
            return $this->redirect()->toRoute('dbpriceandstock/default',array('action'=>'editSuccess'));
        }

        return $array;
    }
    /**
     * 修改历史
     * @return array
     */
    public function logAction()
    {
        $array = array();

        return $array;
    }
    /**
     * 修改成功页面，无任何处理纯展示页面
     * @return array
     */
    public function editSuccessAction()
    {
        $array = array();

        return $array;
    }
    /**
     * 获取商品信息
     * @return ViewModel
     */
    public function goodsInfoAction()
    {
        $view = new ViewModel();
        $view->setTerminal(true);

        $goodsId = (int) $this->request->getPost('goods_id');
        $goodsInfo = $this->getDbshopTable('GoodsTable')->infoGoods(array('dbshop_goods.goods_id'=>$goodsId, 'e.language'=>$this->getDbshopLang()->getLocale()));
        if(empty($goodsInfo)) exit('empty');

        $array = array();
        $array['goods_info'] = $goodsInfo;

        //商品属性组
        $array['attribute_group'] = $this->getDbshopTable('GoodsAttributeGroupTable')->listAttributeGroup(array('e.language'=>$this->getDbshopLang()->getLocale()));

        if($array['goods_info']->goods_spec_type == 2) {//高级规格模式
            //规格组
            $array['goods_spec_tag_group'] = $this->getDbshopTable('GoodsTagGroupTable')->listTagGroup(array('dbshop_goods_tag_group.is_goods_spec'=>1));
            //选中的规格组
            $array['selected_spec_group_id'] = !empty($array['goods_info']->adv_spec_group_id) ? explode(',', $array['goods_info']->adv_spec_group_id) : array();
            //选中的规格
            if(!empty($array['goods_spec_tag_group']) and !empty($array['selected_spec_group_id'])) {
                $specTagArray = array();
                $i = 0;
                foreach($array['goods_spec_tag_group'] as $specTagGroupValue) {
                    if(in_array($specTagGroupValue['tag_group_id'], $array['selected_spec_group_id'])) {
                        $specTagArray[$i]['spec_tag_group_name']    = $specTagGroupValue['tag_group_name'];
                        $specTagArray[$i]['spec_tag_group_id']      = $specTagGroupValue['tag_group_id'];
                        $specTagArray[$i]['spec_tag_value_array']   = $this->getDbshopTable('GoodsTagTable')->simpleListGoodsTag(array('dbshop_goods_tag.tag_group_id'=>$specTagGroupValue['tag_group_id'], 'e.language'=>$this->getDbshopLang()->getLocale()));

                        $selectedSpecTagInfo = $this->getDbshopTable('GoodsAdvSpecGroupTable')->infoGoodsAdvSpecGroup(array('goods_id'=>$goodsId, 'group_id'=>$specTagGroupValue['tag_group_id']));
                        $specTagArray[$i]['selected_spec_tag_id_array'] = (isset($selectedSpecTagInfo->selected_tag_id) and !empty($selectedSpecTagInfo->selected_tag_id)) ? explode(',', $selectedSpecTagInfo->selected_tag_id) : array();

                        $i++;
                    }
                }
                $array['spec_goods_tag_array'] = $specTagArray;
            }
            $array['price_goods_array'] = $this->getDbshopTable('GoodsPriceExtendGoodsTable')->listPriceExtendGoods(array('goods_id'=>$goodsId));
        } else {
            //销售规格，颜色和尺寸
            $array['goods_color']  = $this->getDbshopTable('GoodsPriceExtendTable')->infoPriceExtend(array('extend_type'=>'one','goods_id'=>$goodsId));
            $array['goods_size']   = $this->getDbshopTable('GoodsPriceExtendTable')->infoPriceExtend(array('extend_type'=>'two','goods_id'=>$goodsId));
            //颜色
            $colorArray            = $this->getDbshopTable('GoodsPriceExtendColorTable')->listPriceExtendColor(array('goods_id'=>$goodsId));
            $array['color_list']   = $colorArray;
            $array['color_array']  = array();
            if(is_array($colorArray) and !empty($colorArray)) {
                foreach ($colorArray as $color_val) {
                    $array['color_array'][$color_val['color_value']] = $color_val['color_info'];
                }
            }
            //尺寸
            $sizeArray             = $this->getDbshopTable('GoodsPriceExtendSizeTable')->listPriceExtendSize(array('goods_id'=>$goodsId));
            $array['size_array']   = array();
            if(is_array($sizeArray) and !empty($sizeArray)) {
                foreach ($sizeArray as $size_val) {
                    $array['size_array'][$size_val['size_value']] = $size_val['size_info'];
                }
            }
            //颜色和尺寸混合列表
            $priceGoodsArray            = $this->getDbshopTable('GoodsPriceExtendGoodsTable')->listPriceExtendGoods(array('goods_id'=>$goodsId));
            $array['price_goods_array'] = array();
            if(is_array($priceGoodsArray) and !empty($priceGoodsArray)) {
                foreach ($priceGoodsArray as $goods_val) {
                    $array['price_goods_array'][$goods_val['goods_color']][] = $goods_val;
                }
            }
        }

        return $view->setVariables($array);
    }
    /**
     * 数据表调用
     * @param string $tableName
     * @return multitype:
     */
    private function getDbshopTable ($tableName)
    {
        if (empty($this->dbTables[$tableName])) {
            $this->dbTables[$tableName] = $this->getServiceLocator()->get($tableName);
        }
        return $this->dbTables[$tableName];
    }
}