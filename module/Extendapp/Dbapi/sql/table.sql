DROP TABLE IF EXISTS dbshop_dbapi_userlogin;
CREATE TABLE `dbshop_dbapi_userlogin` (
  `user_id` int(11) NOT NULL,
  `user_token` char(50) NOT NULL,
  `user_serialize` varchar(500) NOT NULL,
  `user_logintime` int(10) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`),
  KEY `user_token` (`user_token`,`user_logintime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='api会员登录表';

DROP TABLE IF EXISTS dbshop_dbapi_ad;
CREATE TABLE `dbshop_dbapi_ad` (
  `dbapi_ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `dbapi_ad_code` char(50) NOT NULL,
  `dbapi_ad_type` char(20) NOT NULL,
  `dbapi_ad_url` varchar(500) DEFAULT NULL,
  `dbapi_ad_body` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`dbapi_ad_id`),
  KEY `dbapi_ad_code` (`dbapi_ad_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS dbshop_dbapi_ad_slide;
CREATE TABLE `dbshop_dbapi_ad_slide` (
  `dbapi_ad_id` int(11) NOT NULL,
  `dbapi_ad_slide_info` varchar(1000) DEFAULT NULL,
  `dbapi_ad_slide_image` char(200) DEFAULT NULL,
  `dbapi_ad_slide_sort` int(3) DEFAULT '255',
  `dbapi_ad_slide_url` varchar(500) DEFAULT NULL,
  KEY `ad_id` (`dbapi_ad_id`,`dbapi_ad_slide_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS dbshop_dbapi_goods;
CREATE TABLE `dbshop_dbapi_goods` (
  `dbapi_goods_id` int(11) NOT NULL AUTO_INCREMENT,
  `dbapi_goods_code` char(50) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `dbapi_goods_sort` int(11) NOT NULL DEFAULT '255',
  PRIMARY KEY (`dbapi_goods_id`),
  KEY `dbapi_goods_code` (`dbapi_goods_code`),
  KEY `dbapi_goods_sort` (`dbapi_goods_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS dbshop_dbapi_cms;
CREATE TABLE `dbshop_dbapi_cms` (
  `dbapi_cms_id` int(11) NOT NULL AUTO_INCREMENT,
  `dbapi_cms_code` char(50) NOT NULL,
  `cms_id` int(11) NOT NULL,
  `dbapi_cms_sort` int(11) NOT NULL DEFAULT '255',
  PRIMARY KEY (`dbapi_cms_id`),
  KEY `dbapi_cms_code` (`dbapi_cms_code`),
  KEY `dbapi_cms_sort` (`dbapi_cms_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;