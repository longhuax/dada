<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2015 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */
return array(
  'Dbapi\Module'                        => __DIR__ . '/Module.php',
  'Dbapi\Controller\IndexController'    => __DIR__ . '/src/Dbapi/Controller/IndexController.php',
  'Dbapi\Controller\JsonapiController'  => __DIR__ . '/src/Dbapi/Controller/JsonapiController.php',
  'Dbapi\Controller\ExtendapiController'  => __DIR__ . '/src/Dbapi/Controller/ExtendapiController.php',
  'Dbapi\Config\apiConfig'                => __DIR__ . '/src/Dbapi/Config/apiConfig.php',

  'Dbapi\Model\ApiGoodsTable'           => __DIR__ . '/src/Dbapi/Model/ApiGoodsTable.php',
  'Dbapi\Model\ApiGoods'                => __DIR__ . '/src/Dbapi/Model/ApiGoods.php',
  'Dbapi\Model\ApiGoodsAskTable'        => __DIR__ . '/src/Dbapi/Model/ApiGoodsAskTable.php',
  'Dbapi\Model\ApiGoodsCommentTable'    => __DIR__ . '/src/Dbapi/Model/ApiGoodsCommentTable.php',
  'Dbapi\Model\ApiGoodsIndexTable'      => __DIR__ . '/src/Dbapi/Model/ApiGoodsIndexTable.php',
  'Dbapi\Model\ApiGoodsIndex'           => __DIR__ . '/src/Dbapi/Model/ApiGoodsIndex.php',
  'Dbapi\Model\ApiCartTable'            => __DIR__ . '/src/Dbapi/Model/ApiCartTable.php',
  'Dbapi\Model\ApiCart'                 => __DIR__ . '/src/Dbapi/Model/ApiCart.php',
  'Dbapi\Model\ApiUserLogin'            => __DIR__ . '/src/Dbapi/Model/ApiUserLogin.php',
  'Dbapi\Model\ApiUserLoginTable'       => __DIR__ . '/src/Dbapi/Model/ApiUserLoginTable.php',
  'Dbapi\Model\ApiOrderTable'           => __DIR__ . '/src/Dbapi/Model/ApiOrderTable.php',
  'Dbapi\Model\ApiFavorites'            => __DIR__ . '/src/Dbapi/Model/ApiFavorites.php',
  'Dbapi\Model\ApiFavoritesTable'       => __DIR__ . '/src/Dbapi/Model/ApiFavoritesTable.php',
    'Dbapi\Model\ApiSetAd'                => __DIR__ . '/src/Dbapi/Model/ApiSetAd.php',
    'Dbapi\Model\ApiSetAdTable'           => __DIR__ . '/src/Dbapi/Model/ApiSetAdTable.php',
    'Dbapi\Model\ApiSetAdSlide'           => __DIR__ . '/src/Dbapi/Model/ApiSetAdSlide.php',
    'Dbapi\Model\ApiSetAdSlideTable'      => __DIR__ . '/src/Dbapi/Model/ApiSetAdSlideTable.php',
    'Dbapi\Model\ApiSetCms'               => __DIR__ . '/src/Dbapi/Model/ApiSetCms.php',
    'Dbapi\Model\ApiSetCmsTable'          => __DIR__ . '/src/Dbapi/Model/ApiSetCmsTable.php',
    'Dbapi\Model\ApiSetGoods'             => __DIR__ . '/src/Dbapi/Model/ApiSetGoods.php',
    'Dbapi\Model\ApiSetGoodsTable'        => __DIR__ . '/src/Dbapi/Model/ApiSetGoodsTable.php',
    'Dbapi\Model\ApiOrderGoodsTable'      => __DIR__ . '/src/Dbapi/Model/ApiOrderGoodsTable.php',
    'Dbapi\Model\ApiUserCouponTable'      => __DIR__ . '/src/Dbapi/Model/ApiUserCouponTable.php',
    'Dbapi\Model\ApiOrderRefundTable'      => __DIR__ . '/src/Dbapi/Model/ApiOrderRefundTable.php',
);
