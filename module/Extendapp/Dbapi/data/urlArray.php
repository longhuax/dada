<?php
return array(
    'adminTools' => array(
        '手机API设置' => array(
            'API基本设置' => '/extendapp/dbapi',
            'API广告设置' => '/extendapp/dbapi/adIndex',
            'API商品设置' => '/extendapp/dbapi/goodsIndex',
            'API文章设置' => '/extendapp/dbapi/cmsIndex'
        )
    )
);