<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Controller;

use Dbapi\Model\DbJsonModel;

/**
 * 自定义接口文件
 * 本文件为空文件，目的是使用者可以在这里自行编写自己需要的接口，在更新api接口时不会被覆盖。
 * Class ExtendapiController
 * @package Dbapi\Controller
 */
class ExtendapiController extends JsonbaseController
{
    private $dbTables = array();
    private $translator;

    /**
     * 详单于测试接口是否成功
     * @return DbJsonModel
     */
    public function indexAction()
    {
        return $this->createJson('success', $this->getDbshopLang()->translate('接口连接成功！'));
    }



    /**
     * 通用分页获取页数和开始值
     * @return array
     */
    private function pageAndQtyAndStart()
    {
        $page   = isset($this->getData['page']) ? ($this->getData['page']   > 0 ? intval($this->getData['page'])    : 1)    : 1;
        $qty    = isset($this->getData['qty'])  ? ($this->getData['qty']    > 0 ? intval($this->getData['qty'])     : 10)   : 10;
        $start  = $page > 1 ? (($page-1) * $qty) : 0;

        return array(
            'limit'     => (int) $qty,
            'offset'    => (int) $start
        );
    }
    /**
     * 生成json数据
     * @param $data
     * @param string $status
     * @param string $message
     * @return DbJsonModel
     */
    private function createJsonData($data, $status='success', $message='')
    {
        $msg    = $status == 'success' ? $this->getDbshopLang()->translate('信息调用成功！') : $this->getDbshopLang()->translate('信息调用失败！');
        if(!empty($message)) $msg = $message;
        $result = new DbJsonModel(array(
            'status'    => $status,
            'msg'       => $msg,
            'result'    => $data
        ));
        return $result;
    }

    /**
     * 抛出json信息
     * @param $status
     * @param $msg
     * @return DbJsonModel
     */
    private function createJson($status, $msg)
    {
        $result = new DbJsonModel(array(
            'status'    => $status,
            'msg'       => $msg
        ));
        return $result;
    }
    /**
     * 数据表调用
     * @param string $tableName
     * @return multitype:
     */
    private function getDbshopTable ($tableName='ApiGoodsTable')
    {
        if (empty($this->dbTables[$tableName])) {
            $this->dbTables[$tableName] = $this->getServiceLocator()->get($tableName);
        }
        return $this->dbTables[$tableName];
    }

    /**
     * 语言包调用
     * @return array|object
     */
    private function getDbshopLang ()
    {
        if (! $this->translator) {
            $this->translator = $this->getServiceLocator()->get('translator');
        }
        return $this->translator;
    }
}