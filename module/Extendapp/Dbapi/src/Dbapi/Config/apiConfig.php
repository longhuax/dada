<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */
namespace Dbapi\Config;

class apiConfig
{
    const WX_APP_ID     = 'wxeeb395e923e575b1';   //小程序的AppID
    const WX_APP_SECRET = 'baf9a2517a41661b312cf98b076fb7c6';   //小程序的appSecret

    const WX_ADD_ORDER_TEMPLATE_ID      = '';   //小程序下单成功消息模板
    const WX_PAY_ORDER_TEMPLATE_ID      = '';   //小程序订单付款完成消息模板
    const WX_FINISH_ORDER_TEMPLATE_ID   = '';   //小程序订单确认完成消息模板
    const WX_CANCEL_ORDER_TEMPLATE_ID   = '';   //取消订单消息模板
}