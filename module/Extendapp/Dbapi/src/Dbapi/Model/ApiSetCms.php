<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

class ApiSetCms
{
    private static $dataArray = array();

    private static function checkData(array $data)
    {
        self::$dataArray['dbapi_cms_id'] = (isset($data['dbapi_cms_id']) and !empty($data['dbapi_cms_id']))
            ? intval($data['dbapi_cms_id'])
            : null;

        self::$dataArray['dbapi_cms_code'] = (isset($data['dbapi_cms_code']) and !empty($data['dbapi_cms_code']))
            ? trim($data['dbapi_cms_code'])
            : null;

        self::$dataArray['cms_id'] = (isset($data['cms_id']) and !empty($data['cms_id']))
            ? intval($data['cms_id'])
            : null;

        self::$dataArray['dbapi_cms_sort'] = (isset($data['dbapi_cms_sort']) and !empty($data['dbapi_cms_sort']))
            ? intval($data['dbapi_cms_sort'])
            : null;

        self::$dataArray = array_filter(self::$dataArray);

        return self::$dataArray;
    }
    /**
     * 添加广告过滤
     * @param array $data
     * @return multitype
     */
    public static function addCmsData(array $data)
    {
        $data = self::checkData($data);

        return $data;
    }
}