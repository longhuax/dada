<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Dbapi\Model\ApiSetGoods as dbshopCheckInData;

class ApiSetGoodsTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_dbapi_goods';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    /**
     * 添加商品
     * @param array $data
     * @return int|null
     */
    public function addApiGoods(array $data)
    {
        $row = $this->insert(dbshopCheckInData::addGoodsData($data));
        if($row) {
            return $this->getLastInsertValue();
        }
        return null;
    }
    /**
     * 获取api商品信息
     * @param array $where
     * @return array|\ArrayObject|null
     */
    public function infoApiGoods(array $where)
    {
        $result = $this->select($where);
        if($result) {
            return $result->current();
        }
        return null;
    }
    /**
     * 获取商品列表（group by 形式）
     * @param array $where
     * @return array|null
     */
    public function listApiGoodsGroup(array $where)
    {
        $result = $this->select(function(Select $select) use ($where){
            $select->where($where);
           $select->group('dbapi_goods_code');
        });
        if($result) {
            return $result->toArray();
        }
        return null;
    }
    /**
     * 更新api商品信息
     * @param array $data
     * @param array $where
     * @return int
     */
    public function updateApiGoods(array $data, array $where)
    {
        return $this->update($data, $where);
    }
    /**
     * 删除api商品信息
     * @param array $where
     * @return int
     */
    public function delApiGoods(array $where)
    {
        return $this->delete($where);
    }
}