<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;


class ApiUserLogin
{
    private static $dataArray = array();

    private static function checkData (array $data)
    {
        self::$dataArray['user_id']         = (isset($data['user_id'])            and !empty($data['user_id']))         ? intval($data['user_id'])      : null;
        self::$dataArray['user_token']      = (isset($data['user_token'])         and !empty($data['user_token']))      ? trim($data['user_token'])   : null;
        self::$dataArray['user_logintime']  = (isset($data['user_logintime'])     and !empty($data['user_logintime']))  ? trim($data['user_logintime']) : time();
        self::$dataArray['user_serialize']  = serialize($data['user_serialize']);

        self::$dataArray = array_filter(self::$dataArray);

        return self::$dataArray;
    }
    /**
     * 添加会员登录过滤
     * @param array $data
     * @return multitype
     */
    public static function addLoginData(array $data)
    {
        $data = self::checkData($data);

        return $data;
    }
}