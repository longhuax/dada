<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class ApiGoodsCommentTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_goods_comment';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter     = $adapter;
        $this->initialize();
    }
    /**
     * 评价列表
     * @param array $array
     * @return mixed
     */
    public function listGoodsComment (array $array)
    {
        $select = new Select(array('dbshop_goods_comment'=>$this->table));
        $where      = isset($array['where']) ? $array['where'] : '';
        $limit      = $array['limit'];
        $offset     = $array['offset'];
        $userAvatar = isset($array['avatar']) ? $array['avatar'] : false;

        if($userAvatar) {
            $select->columns(array('*', new Expression('(SELECT u.user_avatar FROM dbshop_user as u WHERE u.user_name=dbshop_goods_comment.comment_writer) AS user_avatar')));
        }
        $select->where($where)->order('dbshop_goods_comment.comment_time DESC');
        $select->limit($limit);
        $select->offset($offset);

        $resultSet = $this->selectWith($select);
        return $resultSet->toArray();
    }
    /**
     * 商品评价信息
     * @param array $where
     * @return array|\ArrayObject|null
     */
    public function infoGoodsComment (array $where)
    {
        $row = $this->select($where);
        if($row) {
            return $row->current();
        }
        return null;
    }
}