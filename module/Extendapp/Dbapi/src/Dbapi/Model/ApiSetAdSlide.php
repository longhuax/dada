<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

class ApiSetAdSlide
{
    private static $dataArray = array();

    private static function checkData(array $data)
    {
        self::$dataArray['dbapi_ad_id'] = (isset($data['dbapi_ad_id']) and !empty($data['dbapi_ad_id']))
            ? intval($data['dbapi_ad_id'])
            : null;

        self::$dataArray['dbapi_ad_slide_info'] = (isset($data['dbapi_ad_slide_info']) and !empty($data['dbapi_ad_slide_info']))
            ? trim($data['dbapi_ad_slide_info'])
            : null;

        self::$dataArray['dbapi_ad_slide_image'] = (isset($data['dbapi_ad_slide_image']) and !empty($data['dbapi_ad_slide_image']))
            ? trim($data['dbapi_ad_slide_image'])
            : null;

        self::$dataArray['dbapi_ad_slide_sort'] = (isset($data['dbapi_ad_slide_sort']) and !empty($data['dbapi_ad_slide_sort']))
            ? intval($data['dbapi_ad_slide_sort'])
            : null;

        self::$dataArray['dbapi_ad_slide_url'] = (isset($data['dbapi_ad_slide_url']) and !empty($data['dbapi_ad_slide_url']))
            ? trim($data['dbapi_ad_slide_url'])
            : null;

        self::$dataArray = array_filter(self::$dataArray);

        return self::$dataArray;
    }
    /**
     * 添加幻灯片广告过滤
     * @param array $data
     * @return multitype
     */
    public static function addAdSlideData(array $data)
    {
        $data = self::checkData($data);

        return $data;
    }
}