<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

/**
 * 商品信息过滤
 */
class ApiGoods
{
    /**
     * 过滤查询商品信息
     * @param array $data
     * @return array
     */
    public static function whereGoodsData (array $data=array())
    {
        $filter = new \Zend\Filter\HtmlEntities();

        $searchArray = array();
        $searchArray[] = (isset($data['start_goods_id'])        and !empty($data['start_goods_id']))        ? 'dbshop_goods.goods_id >= ' . intval($data['start_goods_id'])            : '';
        $searchArray[] = (isset($data['end_goods_id'])          and !empty($data['end_goods_id']))          ? 'dbshop_goods.goods_id <= ' . intval($data['end_goods_id'])              : '';
        $searchArray[] = (isset($data['goods_item'])            and !empty($data['goods_item']))            ? 'dbshop_goods.goods_item like \'%' . $filter->filter(trim($data['goods_item'])) . '%\''   : '';
        $searchArray[] = (isset($data['start_goods_price'])     and !empty($data['start_goods_price']))     ? 'dbshop_goods.goods_shop_price >= ' . intval($data['start_goods_price']) : '';
        $searchArray[] = (isset($data['end_goods_price'])       and !empty($data['end_goods_price']))       ? 'dbshop_goods.goods_shop_price <= ' . intval($data['end_goods_price'])   : '';
        $searchArray[] = (isset($data['goods_preferential'])    and !empty($data['goods_preferential']))    ? 'dbshop_goods.goods_preferential_price > 0'                              : '';
        //当goods_state 等于-1时，是检查缺货
        $searchArray[] = (isset($data['goods_state'])           and !empty($data['goods_state']))           ? ($data['goods_state'] == -1 ? 'dbshop_goods.goods_stock=0' : 'dbshop_goods.goods_state = ' . intval($data['goods_state'])) : '';
        $searchArray[] = (isset($data['brand_id'])              and !empty($data['brand_id']))              ? 'dbshop_goods.brand_id = ' . intval($data['brand_id'])                   : '';
        $searchArray[] = (isset($data['attribute_group_id'])    and !empty($data['attribute_group_id']))    ? 'dbshop_goods.attribute_group_id = ' . intval($data['attribute_group_id']): '';
        $searchArray[] = (isset($data['goods_type'])            and !empty($data['goods_type']))            ? 'dbshop_goods.goods_type = ' . intval($data['goods_type'])                : '';
        $searchArray[] = (isset($data['goods_integral'])        and !empty($data['goods_integral']))        ? 'dbshop_goods.goods_integral_num>0'                                       : '';
        $searchArray[] = (isset($data['goods_tag_str'])         and !empty($data['goods_tag_str']))         ? $data['goods_tag_str']                                                    : '';

        $searchArray[] = (isset($data['goods_name'])            and !empty($data['goods_name']))            ? self::checkGoodsNameData($filter->filter(trim($data['goods_name'])))      : '';
        $searchArray[] = (isset($data['class_id'])              and !empty($data['class_id']))              ? 'goods_in.class_id = '. intval($data['class_id'])                         : '';
        $searchArray[] = (isset($data['class_recommend'])       and !empty($data['class_recommend']))       ? 'goods_in.class_recommend = '. intval($data['class_recommend'])           : '';
        
        $searchArray[] = (isset($data['tag_id'])                and !empty($data['tag_id']))                ? 'goods_tag_in.tag_id = '. $data['tag_id']                                 : '';

        $searchArray[] = (isset($data['rand_related_goods'])    and !empty($data['rand_related_goods']))    ? trim($data['rand_related_goods'])                                         : '';//用于前台随机显示相关商品

        return array_filter($searchArray);
    }
    /**
     * 对商品名称进行处理，如果有空格则进行or处理
     * @param $goodsName
     * @return string
     */
    public static function checkGoodsNameData($goodsName)
    {
        $goodsNameStr = '';
        $array        = explode(' ', $goodsName);
        foreach($array as $value) {
            $goodsNameStr .= 'e.goods_name like \'%' . $value . '%\' and ';
        }
        if(!empty($goodsNameStr)) return substr($goodsNameStr, 0, -5);
    }
}