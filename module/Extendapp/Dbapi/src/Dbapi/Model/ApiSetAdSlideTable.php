<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Dbapi\Model\ApiSetAdSlide as dbshopCheckInData;

class ApiSetAdSlideTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_dbapi_ad_slide';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    /**
     * 添加幻灯片广告
     * @param array $data
     * @return int|null
     */
    public function addAdSlide(array $data)
    {
        $row = $this->insert(dbshopCheckInData::addAdSlideData($data));
        if($row) {
            return true;
        }
        return null;
    }
    /**
     * 获取幻灯片信息
     * @param array $where
     * @return array|\ArrayObject|null
     */
    public function infoAdSlide(array $where)
    {
        $result = $this->select($where);
        if($result) {
            return $result->current();
        }
        return null;
    }
    /**
     * 获取幻灯片组
     * @param array $where
     * @return array|null|\Zend\Db\ResultSet\ResultSet
     */
    public function listAdSlide(array $where)
    {
        $result = $this->select(function (Select $select) use ($where){
            $select->where($where)->order('dbapi_ad_slide_sort ASC');
        });
        $result = $result->toArray();
        if(!empty($result)) {
            return $result;
        }
        return null;
    }
    /**
     * 幻灯片删除
     * @param array $where
     */
    public function delAdSlide(array $where)
    {
        $adArray = $this->select($where)->toArray();
        if(is_array($adArray) and !empty($adArray)) {
            foreach ($adArray as $adValue) {
                if($adValue['dbapi_ad_slide_image'] != '') @unlink(DBSHOP_PATH . $adValue['dbapi_ad_slide_image']);
            }
        }
        $this->delete($where);
    }
    /**
     * 删除幻灯片单一内容
     * @param array $where
     * @return int
     */
    public function delSlideData(array $where)
    {
        return $this->delete($where);
    }
}