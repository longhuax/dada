<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

class ApiSetGoods
{
    private static $dataArray = array();

    private static function checkData(array $data)
    {
        self::$dataArray['dbapi_goods_id'] = (isset($data['dbapi_goods_id']) and !empty($data['dbapi_goods_id']))
            ? intval($data['dbapi_goods_id'])
            : null;

        self::$dataArray['dbapi_goods_code'] = (isset($data['dbapi_goods_code']) and !empty($data['dbapi_goods_code']))
            ? trim($data['dbapi_goods_code'])
            : null;

        self::$dataArray['goods_id'] = (isset($data['goods_id']) and !empty($data['goods_id']))
            ? intval($data['goods_id'])
            : null;

        self::$dataArray['dbapi_goods_sort'] = (isset($data['dbapi_goods_sort']) and !empty($data['dbapi_goods_sort']))
            ? intval($data['dbapi_goods_sort'])
            : null;

        self::$dataArray = array_filter(self::$dataArray);

        return self::$dataArray;
    }
    /**
     * 添加广告过滤
     * @param array $data
     * @return multitype
     */
    public static function addGoodsData(array $data)
    {
        $data = self::checkData($data);

        return $data;
    }
}