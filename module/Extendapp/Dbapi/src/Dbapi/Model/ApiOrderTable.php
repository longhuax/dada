<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class ApiOrderTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_order';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter     = $adapter;
        $this->initialize();
    }
    /**
     * 订单信息获取
     * @param array $where
     * @param null $limit
     * @return array
     */
    public function allOrder (array $where=array(), $limit=null)
    {
        $result = $this->select(function (Select $select) use ($where, $limit) {
            if(isset($where['search_order_sn']) and $where['search_order_sn'] != '') $select->where->like('order_sn', '%' . $where['search_order_sn'] . '%');
            unset($where['search_order_sn']);

            $select->join(array('a'=>'dbshop_order_delivery_address'), 'a.order_id=dbshop_order.order_id',array('delivery_name'), 'left');
            $select->where($where);
            $select->order('dbshop_order.order_id DESC');
            if(isset($limit) and $limit > 0) $select->limit($limit);
        });
        return $result->toArray();
    }
    /**
     * 前台订单列表
     * @param array $array
     * @return mixed
     */
    public function orderPageList (array $array)
    {
        $select = new Select(array('dbshop_order'=>$this->table));

        $where      = isset($array['where']) ? $array['where'] : '';
        $limit      = $array['limit'];
        $offset     = $array['offset'];

        if(isset($where['search_order_sn']) and $where['search_order_sn'] != '') $select->where->like('order_sn', '%' . $where['search_order_sn'] . '%');
        unset($where['search_order_sn']);

        $select->join(array('a'=>'dbshop_order_delivery_address'), 'a.order_id=dbshop_order.order_id', array('delivery_name'), 'left');

        if(!empty($where)) $select->where($where);
        $select->order('dbshop_order.order_id DESC');

        $select->limit($limit);
        $select->offset($offset);

        $resultSet = $this->selectWith($select);

        return $resultSet->toArray();
    }
}