<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Dbapi\Model\ApiFavorites as dbshopCheckInData;

class ApiFavoritesTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_user_favorites';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter     = $adapter;
        $this->initialize();
    }
    /**
     * 我的收藏列表
     * @param array $array
     * @return mixed
     */
    public function favoritesGoodsListPage(array $array)
    {
        //声明select实例及当前数据表
        $select = new Select(array('dbshop_user_favorites'=>$this->table));

        $where      = isset($array['where']) ? $array['where'] : '';
        $limit      = $array['limit'];
        $offset     = $array['offset'];

        $select->columns(array('*', new Expression('(SELECT i.goods_thumbnail_image FROM dbshop_goods_image as i WHERE i.goods_image_id=e.goods_image_id) as goods_thumbnail_image,(SELECT i.goods_title_image FROM dbshop_goods_image as i WHERE i.goods_image_id=e.goods_image_id) as goods_title_image')));
        $select->join(array('e'=>'dbshop_goods_extend'), 'e.goods_id=dbshop_user_favorites.goods_id', array('goods_name'));
        $select->where($where)->order('dbshop_user_favorites.time DESC');

        $select->limit($limit);
        $select->offset($offset);

        $resultSet = $this->selectWith($select);

        return $resultSet->toArray();
    }
}