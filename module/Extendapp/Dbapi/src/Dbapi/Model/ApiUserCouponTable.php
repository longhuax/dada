<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class ApiUserCouponTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_user_coupon';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter     = $adapter;
        $this->initialize();
    }
    /**
     * 会员优惠券列表
     * @param array $array
     * @return mixed
     */
    public function listUserCouponPage(array $array)
    {
        $select = new Select(array('user_coupon'=>$this->table));
        $where      = isset($array['where']) ? $array['where'] : '';
        $limit      = $array['limit'];
        $offset     = $array['offset'];
        $Sort       = isset($array['order']) ? $array['order'] : 'user_coupon.user_coupon_id DESC';

        $select->join(array('co'=>'dbshop_coupon'), 'co.coupon_id=user_coupon.coupon_id',array('coupon_goods_type', 'coupon_goods_body', 'shopping_discount', 'coupon_use_channel'));

        if(!empty($where)) $select->where($where);
        if(!empty($Sort)) $select->order($Sort);
        $select->limit($limit);
        $select->offset($offset);

        $resultSet = $this->selectWith($select);

        return $resultSet->toArray();
    }
    /**
     * 前台会员优惠券的统计信息
     * @param $userId
     * @return array
     */
    public function allStateNumCoupon($userId)
    {
        $array = array();
        $array['all'] = $this->select(array('user_id'=>$userId))->count();
        $array['0'] = $this->select(array('coupon_use_state'=>0, 'user_id'=>$userId))->count();
        $array['1'] = $this->select(array('coupon_use_state'=>1, 'user_id'=>$userId))->count();
        $array['2'] = $this->select(array('coupon_use_state'=>2, 'user_id'=>$userId))->count();
        $array['3'] = $this->select(array('coupon_use_state'=>3, 'user_id'=>$userId))->count();

        return $array;
    }
}