<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Dbapi\Model\ApiUserLogin as dbshopCheckInData;

class ApiUserLoginTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_dbapi_userlogin';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter     = $adapter;
        $this->initialize();
    }
    /**
     * 添加会员登录信息
     * @param array $data
     * @return int|null
     */
    public function addUserLogin (array $data)
    {
        $row = $this->insert(dbshopCheckInData::addLoginData($data));
        if($row) {
            return true;
        }
        return false;
    }
    /**
     * 获取会员登录信息
     * @param array $where
     * @return array|\ArrayObject|null
     */
    public function infoUserLogin(array $where)
    {
        $result = $this->select($where);
        if($result) {
            return $result->current();
        }
        return null;
    }
    /**
     * 登录信息更新
     * @param array $data
     * @param array $where
     */
    public function updateUserLogin(array $data, array $where)
    {
        $this->update($data, $where);
    }
    /**
     * 删除会员登录信息
     * @param array $where
     */
    public function delUserLogin(array $where)
    {
        $this->delete($where);
    }
}