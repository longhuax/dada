<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

class ApiSetAd
{
    private static $dataArray = array();

    private static function checkData(array $data)
    {
        self::$dataArray['dbapi_ad_id'] = (isset($data['dbapi_ad_id']) and !empty($data['dbapi_ad_id']))
            ? intval($data['dbapi_ad_id'])
            : null;

        self::$dataArray['dbapi_ad_code'] = (isset($data['dbapi_ad_code']) and !empty($data['dbapi_ad_code']))
            ? trim($data['dbapi_ad_code'])
            : null;

        self::$dataArray['dbapi_ad_type'] = (isset($data['dbapi_ad_type']) and !empty($data['dbapi_ad_type']))
            ? trim($data['dbapi_ad_type'])
            : null;

        self::$dataArray['dbapi_ad_url'] = (isset($data['dbapi_ad_url']) and !empty($data['dbapi_ad_url']))
            ? trim($data['dbapi_ad_url'])
            : null;

        self::$dataArray['dbapi_ad_body'] = (isset($data['dbapi_ad_body']) and !empty($data['dbapi_ad_body']))
            ? trim($data['dbapi_ad_body'])
            : null;

        self::$dataArray = array_filter(self::$dataArray);

        return self::$dataArray;
    }
    /**
     * 添加广告过滤
     * @param array $data
     * @return multitype
     */
    public static function addAdData(array $data)
    {
        $data = self::checkData($data);

        return $data;
    }
    /**
     * 更新广告内容
     * @param array $data
     * @return array
     */
    public static function updateAdDate (array $data)
    {
        if(isset($data['dbapi_ad_id'])) unset($data['dbapi_ad_id']);

        return self::checkData($data);
    }
}