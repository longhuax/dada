<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Dbapi\Model\ApiSetCms as dbshopCheckInData;

class ApiSetCmsTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_dbapi_cms';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    /**
     * 添加文章
     * @param array $data
     * @return int|null
     */
    public function addApiCms(array $data)
    {
        $row = $this->insert(dbshopCheckInData::addCmsData($data));
        if($row) {
            return $this->getLastInsertValue();
        }
        return null;
    }
    /**
     * 获取文章信息
     * @param array $where
     * @return array|\ArrayObject|null
     */
    public function infoApiCms(array $where)
    {
        $result = $this->select($where);
        if($result) {
            return $result->current();
        }
        return null;
    }
    /**
     * cms信息列表
     * @param array $where
     * @return mixed
     */
    public function listApiCms(array $where)
    {
        //声明select实例及当前数据表
        $select = new Select(array('cms'=>$this->table));
        $select->join(array('c'=>'dbshop_single_article'), 'c.single_article_id=cms.cms_id');
        $select->join(array('e'=>'dbshop_single_article_extend'), 'e.single_article_id=cms.cms_id',
            array(
                'single_article_title',
                'single_article_body',
                'single_article_title_extend',
                'single_article_keywords',
                'single_article_description'
            )
            );
        if(!empty($where)) $select->where($where);
        $select->order('cms.dbapi_cms_sort ASC');

        $resultSet = $this->selectWith($select);

        return $resultSet->toArray();
    }
    /**
     * cms组形式显示
     * @param array $where
     * @return array|null
     */
    public function listApiCmsGroup(array $where)
    {
        $result = $this->select(function(Select $select) use ($where){
            $select->where($where);
            $select->group('dbapi_cms_sort');
        });
        if($result) {
            return $result->toArray();
        }
        return null;
    }
    /**
     * 更新文章
     * @param array $data
     * @param array $where
     * @return int
     */
    public function updateApiCms(array $data, array $where)
    {
        return $this->update($data, $where);
    }
    /**
     * 删除文章
     * @param array $where
     * @return int
     */
    public function delApiCms(array $where)
    {
        return $this->delete($where);
    }
}