<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

class ApiGoodsIndex
{
    /**
     * where检索
     * @param array $data
     * @return array
     */
    public static function whereGoodsData(array $data)
    {
        $filter = new \Zend\Filter\HtmlEntities();

        $searchArray = array();

        $searchArray[] = (isset($data['start_goods_price'])     and !empty($data['start_goods_price']))     ? 'dbshop_goods.goods_shop_price >= ' . intval($data['start_goods_price']) : '';
        $searchArray[] = (isset($data['end_goods_price'])       and !empty($data['end_goods_price']))       ? 'dbshop_goods.goods_shop_price <= ' . intval($data['end_goods_price'])   : '';
        $searchArray[] = (isset($data['goods_name'])            and !empty($data['goods_name']))            ? self::checkGoodsNameData($filter->filter(trim($data['goods_name'])))      : '';
        $searchArray[] = (isset($data['goods_state'])           and !empty($data['goods_state']))           ? 'dbshop_goods.goods_state = ' . intval($data['goods_state']) : '';

        return array_filter($searchArray);
    }
    /**
     * 对商品名称进行处理，如果有空格则进行or处理
     * @param $goodsName
     * @return string
     */
    public static function checkGoodsNameData($goodsName)
    {
        $goodsNameStr = '';
        $array        = explode(' ', $goodsName);
        foreach($array as $value) {
            $goodsNameStr .= 'dbshop_goods.index_body like \'%' . $value . '%\' and ';
        }
        if(!empty($goodsNameStr)) return substr($goodsNameStr, 0, -5);
    }
}