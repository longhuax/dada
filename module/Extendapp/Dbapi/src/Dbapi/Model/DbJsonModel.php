<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\View\Model\JsonModel as JsonModel;
use Traversable;
use Zend\Stdlib\ArrayUtils as ArrayUtils;

class DbJsonModel extends JsonModel
{
    public function serialize()
    {
        $variables = $this->getVariables();
        if ($variables instanceof Traversable) {
            $variables = ArrayUtils::iteratorToArray($variables);
        }
        if (null !== $this->jsonpCallback) {
            return $this->jsonpCallback.'('.Json::encode($variables).');';
        }
        //JSON_UNESCAPED_UNICODE 参数在PHP5.4中才被加入
        if (version_compare(phpversion(), '5.4', '<') === true) {
            return json_encode($variables);
        }
        return json_encode($variables, JSON_UNESCAPED_UNICODE);
    }
}