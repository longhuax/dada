<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class ApiGoodsAskTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_goods_ask';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter     = $adapter;
        $this->initialize();
    }
    /**
     * 商品咨询列表(分页)
     * @param array $array
     * @return Paginator
     */
    public function listGoodsAsk(array $array)
    {
        $select = new Select(array('dbshop_goods_ask'=>$this->table));
        $where      = isset($array['where']) ? $array['where'] : '';
        $limit      = $array['limit'];
        $offset     = $array['offset'];
        $goodsSort  = isset($array['order']) ? $array['order'] : 'dbshop_goods_ask.ask_time DESC';

        $select->join(array('e'=>'dbshop_goods_extend'), 'e.goods_id=dbshop_goods_ask.goods_id', array('goods_name'));
        $select->join(array('u'=>'dbshop_user'), 'u.user_name=dbshop_goods_ask.ask_writer', array('user_avatar'), 'left');

        if(!empty($where)) $select->where($where);
        if(!empty($goodsSort)) $select->order($goodsSort);
        $select->limit($limit);
        $select->offset($offset);

        $resultSet = $this->selectWith($select);

        return $resultSet->toArray();
    }
    /**
     * 删除商品咨询信息
     * @param array $where
     * @return bool|null
     */
    public function delGoodsAsk(array $where)
    {
        $del = $this->delete($where);
        if($del) {
            return true;
        }
        return null;
    }
}