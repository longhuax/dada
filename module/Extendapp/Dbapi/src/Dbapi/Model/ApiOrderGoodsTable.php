<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2015 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Dbapi\Model\ApiOrderGoods as dbshopCheckInData;

class ApiOrderGoodsTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_order_goods';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter     = $adapter;
        $this->initialize();
    }
    /**
     * 获取订单单个商品信息
     * @param array $where
     * @return array|\ArrayObject|null
     */
    public function infoOrderGoods(array $where)
    {
        $result = $this->select($where);
        if($result) {
            return $result->current();
        }
        return null;
    }
    /**
     * 订单商品列表
     * @param array $array
     * @return mixed
     */
    public function listOrderGoodsPage(array $array)
    {
        $select = new Select(array('order_goods'=>$this->table));
        $where      = isset($array['where']) ? $array['where'] : '';
        $limit      = $array['limit'];
        $offset     = $array['offset'];
        $Sort       = isset($array['order']) ? $array['order'] : 'order_goods.order_goods_id DESC';

        $select->join(array('o'=>'dbshop_order'), 'o.order_id=order_goods.order_id', array('order_sn', 'order_state', 'order_time', 'pay_time', 'express_time', 'finish_time'));

        if(!empty($where)) $select->where($where);
        if(!empty($Sort)) $select->order($Sort);
        $select->limit($limit);
        $select->offset($offset);

        $resultSet = $this->selectWith($select);

        return $resultSet->toArray();
    }
    /**
     * 获取退货商品列表
     * @param array $array
     * @return mixed
     */
    public function listRefundOrderGoodsPage(array $array)
    {
        $select = new Select(array('order_goods'=>$this->table));
        $where      = isset($array['where']) ? $array['where'] : '';
        $limit      = $array['limit'];
        $offset     = $array['offset'];
        $Sort       = isset($array['order']) ? $array['order'] : 'order_goods.order_goods_id DESC';

        $select->join(array('ref'=>'dbshop_order_refund'),'ref.order_id=order_goods.order_id',
            array('order_id', 'order_sn', 'refund_state', 'refund_time', 'finish_refund_time', 'refund_info', 're_refund_info'),
            'left'
            );

        if(!empty($where)) $select->where($where);
        if(!empty($Sort)) $select->order($Sort);
        $select->limit($limit);
        $select->offset($offset);

        $resultSet = $this->selectWith($select);

        return $resultSet->toArray();
    }
}