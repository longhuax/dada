<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbapi\Event;

use Zend\EventManager\Event;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

class Listener implements ListenerAggregateInterface
{
    protected $listeners = array();

    /**
     * @param EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events)
    {
        $shareEvents = $events->getSharedManager();

        //后台
        $this->listeners[] = $shareEvents->attach('User\Controller\UserController', 'user.save.backstage.post',
            array($this, 'onUpdateApiLoginUser')
        );
        $this->listeners[] = $shareEvents->attach('User\Controller\UserController', 'user.del.backstage.post',
            array($this, 'onDelApiLoginUser')
        );
        $this->listeners[] = $shareEvents->attach('User\Controller\UserController', 'user.alldel.backstage.post',
            array($this, 'onDelAllApiLoginUser')
        );

    }
    /**
     * @param EventManagerInterface $events
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    /**
     * 更新api的登录信息
     * @param Event $e
     */
    public function onUpdateApiLoginUser(Event $e)
    {
        $values = $e->getParam('values');
        if($values['user_id'] > 0) {
            $apiUserLoginTable = $e->getTarget()->getServiceLocator()->get('ApiUserLoginTable');
            $userTable = $e->getTarget()->getServiceLocator()->get('UserTable');
            $userGroupTable = $e->getTarget()->getServiceLocator()->get('UserGroupExtendTable');

            $userLoginInfo = $apiUserLoginTable->infoUserLogin(array('user_id'=>$values['user_id']));
            $userInfo = $userTable->infoUser(array('user_id'=>$values['user_id']));
            if(!empty($userInfo) and !empty($userLoginInfo)) {
                $userGroup = $userGroupTable->infoUserGroupExtend(array('group_id'=>$userInfo->group_id));
                if(!empty($userGroup)) {
                    $user_serialize = array(
                        'user_name'      => $userInfo->user_name,
                        'user_id'        => $userInfo->user_id,
                        'user_email'     => $userInfo->user_email,
                        'user_phone'     => $userInfo->user_phone,
                        'group_id'       => $userInfo->group_id,
                        'user_group_name'=> $userGroup->group_name,
                        'user_avatar'    => (!empty($userInfo->user_avatar) ? $userInfo->user_avatar : $e->getTarget()->getServiceLocator()->get('frontHelper')->getUserIni('default_avatar'))
                    );
                    $userLogin = array(
                        'user_serialize'=> serialize($user_serialize)
                    );
                    //更新登录信息存储
                    $apiUserLoginTable->updateUserLogin($userLogin, array('user_id'=>$userInfo->user_id));
                    //如果是待审核或者关闭状态，则删除api中的登录
                    if(isset($values['user_state']) and in_array($values['user_state'], array(2,3))) {
                        $apiUserLoginTable->delUserLogin(array('user_id'=>$userInfo->user_id));
                    }
                }
            }
        }
    }
    /**
     * 删除会员登录信息
     * @param Event $e
     */
    public function onDelApiLoginUser(Event $e)
    {
        $values = $e->getParam('values');
        if($values->user_id > 0) {
            $apiUserLoginTable = $e->getTarget()->getServiceLocator()->get('ApiUserLoginTable');
            $apiUserLoginTable->delUserLogin(array('user_id'=>$values->user_id));
        }
    }
    /**
     * 批量删除会员登录信息
     * @param Event $e
     */
    public function onDelAllApiLoginUser(Event $e)
    {
        $values = $e->getParam('values');
        if(is_array($values) and !empty($values)) {
            $apiUserLoginTable = $e->getTarget()->getServiceLocator()->get('ApiUserLoginTable');
            foreach($values as $userId) {
                $apiUserLoginTable->delUserLogin(array('user_id'=>$userId));
            }
        }
    }

}