<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2015 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Dbapi\Controller\Index' => 'Dbapi\Controller\IndexController',
            'Dbapi\Controller\Jsonapi' => 'Dbapi\Controller\JsonapiController',
            'Dbapi\Controller\Extendapi' => 'Dbapi\Controller\ExtendapiController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'dbapi'     => include __DIR__ . '/router/router.config.php',
            'jsonapi'   => include __DIR__ . '/router/jsonapi.router.php',
            'extendapi'   => include __DIR__ . '/router/extendapi.router.php',
        ),
    ),
    'translator' => array(
            'translation_file_patterns' => array(
                    array(
                            'type' => 'gettext',
                            'base_dir' => __DIR__ . '/../language',
                            'pattern' => '%s.mo'
                    )
            )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Dbapi' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy'
        ),
    ),
);
