<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2016 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

return array(
    'ApiGoodsTable'            => function  () { return new \Dbapi\Model\ApiGoodsTable();            },
    'ApiGoodsAskTable'         => function  () { return new \Dbapi\Model\ApiGoodsAskTable();         },
    'ApiGoodsCommentTable'     => function  () { return new \Dbapi\Model\ApiGoodsCommentTable();     },
    'ApiGoodsIndexTable'       => function  () { return new \Dbapi\Model\ApiGoodsIndexTable();       },
    'ApiCartTable'             => function  () { return new \Dbapi\Model\ApiCartTable();             },
    'ApiUserLoginTable'        => function  () { return new \Dbapi\Model\ApiUserLoginTable();        },
    'ApiOrderTable'            => function  () { return new \Dbapi\Model\ApiOrderTable();            },
    'ApiFavoritesTable'        => function  () { return new \Dbapi\Model\ApiFavoritesTable();        },
    'ApiSetAdTable'            => function  () { return new \Dbapi\Model\ApiSetAdTable();            },
    'ApiSetAdSlideTable'       => function  () { return new \Dbapi\Model\ApiSetAdSlideTable();       },
    'ApiSetCmsTable'           => function  () { return new \Dbapi\Model\ApiSetCmsTable();           },
    'ApiSetGoodsTable'         => function  () { return new \Dbapi\Model\ApiSetGoodsTable();         },
    'ApiOrderGoodsTable'       => function  () { return new \Dbapi\Model\ApiOrderGoodsTable();       },
    'ApiUserCouponTable'       => function  () { return new \Dbapi\Model\ApiUserCouponTable();       },
    'ApiOrderRefundTable'       => function  () { return new \Dbapi\Model\ApiOrderRefundTable();       },
);