<?php
return array(
    'adminOperate' => array(
        '分销管理' => array(
            '分销设置' => '/extendapp/distribution',
            '次级分销用户' => '/extendapp/distribution/userList',
            '顶级分销用户' => '/extendapp/distribution/topUserList',
            '特定商品分销' => '/extendapp/distribution/specDistributionGoods',
        )
    )
);