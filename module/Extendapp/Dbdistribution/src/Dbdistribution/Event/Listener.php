<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbdistribution\Event;

use Zend\Config\Reader\Ini;
use Zend\EventManager\Event;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Session\Container;

class Listener implements ListenerAggregateInterface
{
    protected $listeners = array();

    public function __construct()
    {

    }

    public function attach(EventManagerInterface $events)
    {
        $shareEvents = $events->getSharedManager();

        $this->listeners[] = $shareEvents->attach(
            'Dbapi\Controller\JsonapiController',
            'user.register.front.post',
            array($this, 'onAddDistributionUser')
        );

        $this->listeners[] = $shareEvents->attach(
            array(
                'Shopfront\Controller\UserController',
                'Mobile\Controller\UserController'
            ),
            'user.register.front.post',
            array($this, 'onAddPcAndPhoneDistributionUser')
        );

        $this->listeners[] = $shareEvents->attach(
            array(
                'Shopfront\Controller\CartController',
                'Mobile\Controller\CartController',
                'Dbapi\Controller\JsonapiController'
            ),
            'cart.submit.front.post',
            array($this, 'onAddDistributionOrder')
        );

        $this->listeners[] = $shareEvents->attach(
            'User\Controller\UserController',
            'user.del.backstage.post',
            array($this, 'onDelDistributionUser')
        );

        $this->listeners[] = $shareEvents->attach(
            'User\Controller\UserController',
            'user.alldel.backstage.post',
            array($this, 'onAllDelDistributionUser')
        );
        $this->listeners[] = $shareEvents->attach(//订单价格修改时，同步修改对应订单的提成
            'Orders\Controller\OrdersController',
            'order.changeAmount.backstage.post',
            array($this, 'onChangeDistributionOrder')
        );
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    /**
     * 删除分销用户
     * @param Event $e
     */
    public function onDelDistributionUser(Event $e)
    {
        $values = $e->getParam('values');
        if(isset($values->user_id) and $values->user_id > 0) {
            $e->getTarget()->getServiceLocator()->get('DistributionUserTable')->delDistributionUser(array('user_id'=>$values->user_id));
            $e->getTarget()->getServiceLocator()->get('DistributionOrderTable')->delDistributionOrder(array('buy_user_id'=>$values->user_id, 'o_state'=>2));
        }
    }

    /**
     * 批量删除
     * @param Event $e
     */
    public function onAllDelDistributionUser(Event $e)
    {
        $values = $e->getParam('values');
        if(is_array($values) and !empty($values)) {
            $e->getTarget()->getServiceLocator()->get('DistributionUserTable')->delDistributionUser(array('user_id IN ('.implode(',', $values).')'));
            $e->getTarget()->getServiceLocator()->get('DistributionOrderTable')->delDistributionOrder(array('buy_user_id IN ('.implode(',', $values).') and o_state=2'));
        }
    }

    /**
     * pc端和phone端的处理
     * @param Event $e
     */
    public function onAddPcAndPhoneDistributionUser(Event $e)
    {
        $shareSession = new Container('share');
        $values = $e->getParam('values');

        $distributionUserId = isset($shareSession->distribution_user_id) ? (int) $shareSession->distribution_user_id : 0;

        if($distributionUserId > 0) {
            $userInfo = $e->getTarget()->getServiceLocator()->get('UserTable')->infoUser(array('user_id'=>$distributionUserId));
            if($userInfo) {
                //新的分销用户记录添加
                $addDistributionUser = array(
                    'user_id' => $values['user_id'],
                    'user_name' => $values['user_name'],
                    'one_level_user_id' => $userInfo->user_id,
                    'one_level_user_name' => $userInfo->user_name
                );

                //检查上级用户是否还用上级
                $distributionUser = $e->getTarget()->getServiceLocator()->get('DistributionUserTable')->infoDistributionUser(array('user_id'=>$distributionUserId));
                if($distributionUser) {
                    $addDistributionUser['top_level_user_id'] = $distributionUser->one_level_user_id;
                    $addDistributionUser['top_level_user_name'] = $distributionUser->one_level_user_name;
                }

                $e->getTarget()->getServiceLocator()->get('DistributionUserTable')->addDistributionUser($addDistributionUser);
            }
        }
    }

    /**
     * 添加分销用户
     * @param Event $e
     */
    public function onAddDistributionUser(Event $e)
    {
        $values      = $e->getParam('values');
        $distributionUserId = $e->getTarget()->getServiceLocator()->get('request')->getPost('distribution_user_id');

        if($distributionUserId > 0) {
            $userInfo = $e->getTarget()->getServiceLocator()->get('UserTable')->infoUser(array('user_id'=>$distributionUserId));
            if($userInfo) {
                //新的分销用户记录添加
                $addDistributionUser = array(
                    'user_id' => $values['user_id'],
                    'user_name' => $values['user_name'],
                    'one_level_user_id' => $userInfo->user_id,
                    'one_level_user_name' => $userInfo->user_name
                );

                //检查上级用户是否还用上级
                $distributionUser = $e->getTarget()->getServiceLocator()->get('DistributionUserTable')->infoDistributionUser(array('user_id'=>$distributionUserId));
                if($distributionUser) {
                    $addDistributionUser['top_level_user_id'] = $distributionUser->one_level_user_id;
                    $addDistributionUser['top_level_user_name'] = $distributionUser->one_level_user_name;
                }

                $e->getTarget()->getServiceLocator()->get('DistributionUserTable')->addDistributionUser($addDistributionUser);
            }
        }
    }

    /**
     * 添加分销提成信息
     * @param Event $e
     */
    public function onAddDistributionOrder(Event $e)
    {
        $other = $e->getParam('other');
        if(empty($other) or !is_array($other)) return;

        $configPath = DBSHOP_PATH . '/module/Extendapp/Dbdistribution/data/config.ini';
        if(!file_exists($configPath)) return;

        $iniRead = new Ini();
        $config = $iniRead->fromFile($configPath);
        if(isset($config['distribution_state']) && $config['distribution_state'] == 1) {//启用状态下执行
            //检查是否有分销上级
            $distributionUser = $e->getTarget()->getServiceLocator()->get('DistributionUserTable')->infoDistributionUser(array('user_id'=>$other['user_id']));
            if(!$distributionUser) return;

            $firstMoney  = 0;
            $twoTopMoney = 0;

            $orderInfo = $e->getTarget()->getServiceLocator()->get('OrderTable')->infoOrder(array('order_id'=>$other['order_id']));

            if($config['distribution_first'] == 0 && $config['distribution_two_top'] == 0) {//特定分销商品
                $orderGoods = $e->getTarget()->getServiceLocator()->get('OrderGoodsTable')->listOrderGoods(array('order_id'=>$other['order_id']));
                foreach ($orderGoods as $goodsValue) {
                    $distributionGoods = $e->getTarget()->getServiceLocator()->get('DistributionGoodsTable')->infoDistributionGoods(array('goods_id'=>$goodsValue['goods_id']));
                    if($distributionGoods) {
                        if($distributionGoods->distribution_first_type == '%') $firstMoney = $firstMoney + round($goodsValue['goods_amount'] * ($distributionGoods->distribution_first / 100), 2);
                        else $firstMoney = $firstMoney + $distributionGoods->distribution_first * $goodsValue['buy_num'];

                        if($distributionGoods->distribution_two_type == '%') $twoTopMoney = $twoTopMoney + round($goodsValue['goods_amount'] * ($distributionGoods->distribution_two / 100), 2);
                        else $twoTopMoney = $twoTopMoney + $distributionGoods->distribution_two * $goodsValue['buy_num'];
                    }
                }
            } else {//统一分销商品
                $baseMoney = $orderInfo->order_amount - $orderInfo->express_fee - $orderInfo->pay_fee;
                //上级分销商获取的提成，当上面只有一级分销商时，使用
                if($config['distribution_first_type'] == '%') $firstMoney = round($baseMoney * ($config['distribution_first'] / 100), 2);
                else $firstMoney = $config['distribution_first'];

                //上级分销商的上级
                if($config['distribution_two_top_type'] == '%')$twoTopMoney = round($baseMoney * ($config['distribution_two_top'] / 100), 2);
                else $twoTopMoney = $config['distribution_two_top'];
            }

            if($firstMoney > 0 || $twoTopMoney > 0) {
                $addDistributionOrder = array(
                    'order_id'  => $orderInfo->order_id,
                    'order_sn'  => $orderInfo->order_sn,
                    'buy_user_id'   => $distributionUser->user_id,
                    'buy_user_name' => $distributionUser->user_name,
                    'one_level_user_id'     => $distributionUser->one_level_user_id,
                    'one_level_user_name'   => $distributionUser->one_level_user_name,
                );
                if($distributionUser->top_level_user_id > 0) {
                    $addDistributionOrder['one_level_user_cost'] = $firstMoney;
                    $addDistributionOrder['top_level_user_id']   = $distributionUser->top_level_user_id;
                    $addDistributionOrder['top_level_user_name'] = $distributionUser->top_level_user_name;
                    $addDistributionOrder['top_level_user_cost'] = $twoTopMoney;
                } else {
                    $addDistributionOrder['one_level_user_cost'] = $firstMoney;
                }
                $e->getTarget()->getServiceLocator()->get('DistributionOrderTable')->addDistributionOrder($addDistributionOrder);
            }

        }
    }

    /**
     * 后台修改订单金额，分成同步修改
     * @param Event $e
     */
    public function onChangeDistributionOrder(Event $e)
    {
        $values = $e->getParam('values');

        $distributionOrderTable = $e->getTarget()->getServiceLocator()->get('DistributionOrderTable');

        $distributionOrderInfo = $distributionOrderTable->infoDistributionOrder(array('order_id'=>$values['order_id']));
        if($distributionOrderInfo && $distributionOrderInfo->o_state == 2) {
            $configPath = DBSHOP_PATH . '/module/Extendapp/Dbdistribution/data/config.ini';
            if(!file_exists($configPath)) return;

            $iniRead = new Ini();
            $config = $iniRead->fromFile($configPath);
            if(isset($config['distribution_state']) && $config['distribution_state'] == 1) {//启用状态下执行

                $firstMoney  = 0;
                $twoTopMoney = 0;

                if($config['distribution_first'] > 0 || $config['distribution_two_top'] > 0) {//不能对特定商品配型，进行处理。因为并无法计算哪种商品调价了。
                    $baseMoney = $values['order_edit_amount'];
                    //上级分销商获取的提成，当上面只有一级分销商时，使用
                    if($config['distribution_first_type'] == '%') $firstMoney = round($baseMoney * ($config['distribution_first'] / 100), 2);
                    else $firstMoney = $config['distribution_first'];

                    //上级分销商的上级
                    if($config['distribution_two_top_type'] == '%')$twoTopMoney = round($baseMoney * ($config['distribution_two_top'] / 100), 2);
                    else $twoTopMoney = $config['distribution_two_top'];

                    $updateDistributionOrder = array();
                    if($distributionOrderInfo->top_level_user_id > 0) {
                        $updateDistributionOrder['one_level_user_cost'] = $firstMoney;
                        $updateDistributionOrder['top_level_user_cost'] = $twoTopMoney;
                    } else {
                        $updateDistributionOrder['one_level_user_cost'] = $firstMoney;
                    }

                    $distributionOrderTable->updateDistributionOrder($updateDistributionOrder, array('order_id'=>$values['order_id']));
                }

            }
        }
    }
}