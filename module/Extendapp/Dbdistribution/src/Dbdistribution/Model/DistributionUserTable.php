<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbdistribution\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\AbstractTableGateway;
use Dbdistribution\Model\DistributionUser as dbshopCheckInData;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class DistributionUserTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_distribution_user';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }

    /**
     * 添加分销用户
     * @param array $data
     * @return int|null
     */
    public function addDistributionUser(array $data)
    {
        $row = $this->insert(dbshopCheckInData::addDistributionUserData($data));
        if($row) {
            return $this->getLastInsertValue();
        }
        return null;
    }

    /**
     * 分销用户信息
     * @param array $where
     * @return array|\ArrayObject|null
     */
    public function infoDistributionUser(array $where)
    {
        $result = $this->select($where);
        if($result) {
            return $result->current();
        }
        return null;
    }
    public function topDistributionUserList(array $pageArray, array $where=array())
    {
        $select = new Select(array('u'=>'dbshop_user'));
        $sWhere  = dbshopCheckInData::whereDistributionUserData($where);
        $sHaving = dbshopCheckInData::havingDistributionUserData($where);

        $select->columns(array('*', new Expression('
                        (SELECT COALESCE(SUM(o.one_level_user_cost),0) FROM dbshop_distribution_order AS o WHERE o.one_level_user_id=u.user_id and o.o_state=1) AS f1_cost,
        (SELECT COALESCE(SUM(o1.top_level_user_cost),0) FROM dbshop_distribution_order AS o1 WHERE o1.top_level_user_id=u.user_id and o1.o_state=1) AS f2_cost,
        (SELECT COALESCE(SUM(o2.one_level_user_cost),0) FROM dbshop_distribution_order AS o2 WHERE o2.one_level_user_id=u.user_id and o2.o_state=2) AS n1_cost,
        (SELECT COALESCE(SUM(o3.top_level_user_cost),0) FROM dbshop_distribution_order AS o3 WHERE o3.top_level_user_id=u.user_id and o3.o_state=2) AS n2_cost,
		(SELECT COALESCE(SUM(l3.money_change_num),0) FROM dbshop_withdraw_log AS l3 WHERE l3.user_id=u.user_id and l3.withdraw_state=1) AS al_cost,
		(SELECT COALESCE(SUM(d3.money_change_num),0) FROM dbshop_withdraw_log AS d3 WHERE d3.user_id=u.user_id and d3.withdraw_state=0) AS ing_cost,
		(SELECT user_money FROM dbshop_user AS u3 WHERE u3.user_id=u.user_id) AS u_cost,
        (SELECT COUNT(u2.user_id) FROM dbshop_distribution_user AS u2 WHERE u2.one_level_user_id=u.user_id) AS one_num,
        (SELECT COUNT(u3.user_id) FROM dbshop_distribution_user AS u3 WHERE u3.top_level_user_id=u.user_id) AS two_num
        ')));

        if(!empty($sWhere)) $select->where($sWhere);
        if(!empty($sHaving)) $select->having($sHaving);
        $select->where('u.group_id=2 and u.user_id NOT IN (SELECT u1.user_id FROM '.$this->table.' AS u1)');
        $select->order('f1_cost DESC, f2_cost DESC, n1_cost DESC, n2_cost DESC');

        //实例化分页处理
        $pageAdapter = new DbSelect($select, $this->adapter);
        $paginator   = new Paginator($pageAdapter);
        $paginator->setCurrentPageNumber($pageArray['page']);
        $paginator->setItemCountPerPage(50);

        return $paginator;
    }
    /**
     * 分销用户列表
     * @param array $pageArray
     * @param array $where
     * @return Paginator
     */
    public function distributionUserList(array $pageArray, array $where=array())
    {
        $select = new Select(array('u'=>$this->table));
        $sWhere  = dbshopCheckInData::whereDistributionUserData($where);
        $sHaving = dbshopCheckInData::havingDistributionUserData($where);

        $select->columns(array('*', new Expression('
                (SELECT COALESCE(SUM(o.one_level_user_cost),0) FROM dbshop_distribution_order AS o WHERE o.one_level_user_id=u.user_id and o.o_state=1) AS f1_cost,
        (SELECT COALESCE(SUM(o1.top_level_user_cost),0) FROM dbshop_distribution_order AS o1 WHERE o1.top_level_user_id=u.user_id and o1.o_state=1) AS f2_cost,
        (SELECT COALESCE(SUM(o2.one_level_user_cost),0) FROM dbshop_distribution_order AS o2 WHERE o2.one_level_user_id=u.user_id and o2.o_state=2) AS n1_cost,
        (SELECT COALESCE(SUM(o3.top_level_user_cost),0) FROM dbshop_distribution_order AS o3 WHERE o3.top_level_user_id=u.user_id and o3.o_state=2) AS n2_cost,
        (SELECT COUNT(u2.user_id) FROM dbshop_distribution_user AS u2 WHERE u2.one_level_user_id=u.user_id) AS one_num,
        (SELECT COUNT(u3.user_id) FROM dbshop_distribution_user AS u3 WHERE u3.top_level_user_id=u.user_id) AS two_num
        ')));
        if(!empty($sWhere)) $select->where($sWhere);
        if(!empty($sHaving)) $select->having($sHaving);
        $select->order('one_num DESC');
        //实例化分页处理
        $pageAdapter = new DbSelect($select, $this->adapter);
        $paginator   = new Paginator($pageAdapter);
        $paginator->setCurrentPageNumber($pageArray['page']);
        $paginator->setItemCountPerPage($pageArray['page_num']);

        return $paginator;
    }

    /**
     * 分销商下面的用户
     * @param array $pageArray
     * @param array $where
     * @return Paginator
     */
    public function subDistributionUserList(array $pageArray, array $where=array(), $userId)
    {
        $select = new Select(array('u'=>$this->table));

        $select->columns(array('*', new Expression('
                (SELECT COALESCE(SUM(o.one_level_user_cost),0) FROM dbshop_distribution_order AS o WHERE o.one_level_user_id='.$userId.' and o.buy_user_id=u.user_id and o.o_state=1) AS f1_cost,
        (SELECT COALESCE(SUM(o1.top_level_user_cost),0) FROM dbshop_distribution_order AS o1 WHERE o1.top_level_user_id='.$userId.' and o1.buy_user_id=u.user_id and o1.o_state=1) AS f2_cost,
        (SELECT COALESCE(SUM(o2.one_level_user_cost),0) FROM dbshop_distribution_order AS o2 WHERE o2.one_level_user_id='.$userId.' and o2.buy_user_id=u.user_id and o2.o_state=2) AS n1_cost,
        (SELECT COALESCE(SUM(o3.top_level_user_cost),0) FROM dbshop_distribution_order AS o3 WHERE o3.top_level_user_id='.$userId.' and o3.buy_user_id=u.user_id and o3.o_state=2) AS n2_cost
        ')));
        if(!empty($where)) $select->where($where);
        $select->order('u.u_id DESC');
        //实例化分页处理
        $pageAdapter = new DbSelect($select, $this->adapter);
        $paginator   = new Paginator($pageAdapter);
        $paginator->setCurrentPageNumber($pageArray['page']);
        $paginator->setItemCountPerPage($pageArray['page_num']);

        return $paginator;
    }

    /**
     * 分销用户列表
     * @param array $array
     * @return mixed
     */
    public function apiDistributionUserList(array $array)
    {
        $select = new Select(array('u'=>$this->table));
        $where      = isset($array['where']) ? $array['where'] : '';
        $limit      = $array['limit'];
        $offset     = $array['offset'];
        $Sort       = isset($array['order']) ? $array['order'] : 'u_id DESC';

        $select->columns(array('*', new Expression('
        (SELECT COALESCE(SUM(o.one_level_user_cost),0) FROM dbshop_distribution_order AS o WHERE o.one_level_user_id='.$array['user_id'].' and o.buy_user_id=u.user_id and o.o_state=1) AS f1_cost,
        (SELECT COALESCE(SUM(o1.top_level_user_cost),0) FROM dbshop_distribution_order AS o1 WHERE o1.top_level_user_id='.$array['user_id'].' and o1.buy_user_id=u.user_id and o1.o_state=1) AS f2_cost,
        (SELECT COALESCE(SUM(o2.one_level_user_cost),0) FROM dbshop_distribution_order AS o2 WHERE o2.one_level_user_id='.$array['user_id'].' and o2.buy_user_id=u.user_id and o2.o_state=2) AS n1_cost,
        (SELECT COALESCE(SUM(o3.top_level_user_cost),0) FROM dbshop_distribution_order AS o3 WHERE o3.top_level_user_id='.$array['user_id'].' and o3.buy_user_id=u.user_id and o3.o_state=2) AS n2_cost
        ')));
        if(!empty($where)) $select->where($where);
        if(!empty($Sort)) $select->order($Sort);
        $select->limit($limit);
        $select->offset($offset);

        $resultSet = $this->selectWith($select);

        return $resultSet->toArray();
    }

    /**
     * 获取总人数
     * @param $where
     * @return int
     */
    public function distributionUserTotal($where)
    {
        return $this->select($where)->count();
    }

    /**
     * 删除分销用户，同时删除分销用户的订单分成信息
     * @param $where
     */
    public function delDistributionUser($where)
    {
        $del = $this->delete($where);
        if($del) {
            return true;
        }
        return null;
    }
}