<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbdistribution\Model;


class DistributionOrder
{
    private static $dataArray = array();

    private static function checkData (array $data)
    {
        self::$dataArray['o_id'] = (isset($data['o_id']) and !empty($data['o_id']))
            ? intval($data['o_id'])
            : null;

        self::$dataArray['o_state'] = (isset($data['o_state']) and !empty($data['o_state']))
            ? intval($data['o_state'])
            : null;

        self::$dataArray['o_finish_time'] = (isset($data['o_finish_time']) and !empty($data['o_finish_time']))
            ? intval($data['o_finish_time'])
            : null;

        self::$dataArray['order_id'] = (isset($data['order_id']) and !empty($data['order_id']))
            ? intval($data['order_id'])
            : null;

        self::$dataArray['order_sn'] = (isset($data['order_sn']) and !empty($data['order_sn']))
            ? trim($data['order_sn'])
            : null;

        self::$dataArray['buy_user_id'] = (isset($data['buy_user_id']) and !empty($data['buy_user_id']))
            ? intval($data['buy_user_id'])
            : null;

        self::$dataArray['buy_user_name'] = (isset($data['buy_user_name']) and !empty($data['buy_user_name']))
            ? trim($data['buy_user_name'])
            : null;

        self::$dataArray['one_level_user_id'] = (isset($data['one_level_user_id']) and !empty($data['one_level_user_id']))
            ? intval($data['one_level_user_id'])
            : null;

        self::$dataArray['one_level_user_name'] = (isset($data['one_level_user_name']) and !empty($data['one_level_user_name']))
            ? trim($data['one_level_user_name'])
            : null;

        self::$dataArray['one_level_user_cost'] = (isset($data['one_level_user_cost']) and !empty($data['one_level_user_cost']))
            ? trim($data['one_level_user_cost'])
            : null;

        self::$dataArray['top_level_user_id'] = (isset($data['top_level_user_id']) and !empty($data['top_level_user_id']))
            ? intval($data['top_level_user_id'])
            : null;

        self::$dataArray['top_level_user_name'] = (isset($data['top_level_user_name']) and !empty($data['top_level_user_name']))
            ? trim($data['top_level_user_name'])
            : null;

        self::$dataArray['top_level_user_cost'] = (isset($data['top_level_user_cost']) and !empty($data['top_level_user_cost']))
            ? trim($data['top_level_user_cost'])
            : null;

        self::$dataArray = array_filter(self::$dataArray);


        return self::$dataArray;
    }

    public static function addDistributionOrderData(array $data)
    {
        $data = self::checkData($data);
        return $data;
    }
}