<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbdistribution\Model;


use Zend\Filter\HtmlEntities;

class DistributionUser
{
    private static $dataArray = array();

    private static function checkData (array $data)
    {
        self::$dataArray['u_id'] = (isset($data['u_id']) and !empty($data['u_id']))
            ? intval($data['u_id'])
            : null;

        self::$dataArray['user_id'] = (isset($data['user_id']) and !empty($data['user_id']))
            ? intval($data['user_id'])
            : null;

        self::$dataArray['user_name'] = (isset($data['user_name']) and !empty($data['user_name']))
            ? trim($data['user_name'])
            : null;

        self::$dataArray['one_level_user_id'] = (isset($data['one_level_user_id']) and !empty($data['one_level_user_id']))
            ? intval($data['one_level_user_id'])
            : null;

        self::$dataArray['one_level_user_name'] = (isset($data['one_level_user_name']) and !empty($data['one_level_user_name']))
            ? trim($data['one_level_user_name'])
            : null;

        self::$dataArray['top_level_user_id'] = (isset($data['top_level_user_id']) and !empty($data['top_level_user_id']))
            ? intval($data['top_level_user_id'])
            : null;

        self::$dataArray['top_level_user_name'] = (isset($data['top_level_user_name']) and !empty($data['top_level_user_name']))
            ? trim($data['top_level_user_name'])
            : null;

        self::$dataArray = array_filter(self::$dataArray);


        return self::$dataArray;
    }

    public static function addDistributionUserData($data)
    {
        $data = self::checkData($data);
        return $data;
    }

    public static function whereDistributionUserData($data=null)
    {
        $filter = new HtmlEntities();
        $searchArray = array();

        $searchArray[] = (isset($data['user_name']) and !empty($data['user_name'])) ? 'u.user_name LIKE \'%'.$filter->filter($data['user_name']).'%\'' : '';
        $searchArray[] = (isset($data['level']) and !empty($data['level'])) ? ($data['level'] == 1 ? 'u.top_level_user_id<=0' : 'u.top_level_user_id>0') : '';
        $searchArray[] = (isset($data['one_level_user_name']) and !empty($data['one_level_user_name'])) ? 'u.one_level_user_name LIKE \'%'.$filter->filter($data['one_level_user_name']).'%\'' : '';
        $searchArray[] = (isset($data['top_level_user_name']) and !empty($data['top_level_user_name'])) ? 'u.one_level_user_name LIKE \'%'.$filter->filter($data['top_level_user_name']).'%\'' : '';

        return array_filter($searchArray);
    }

    public static function havingDistributionUserData($data=null)
    {
        $filter = new HtmlEntities();
        $searchArray = array();

        $searchArray[] = (isset($data['search_start_f_cost']) and !empty($data['search_start_f_cost'])) ? 'f1_cost+f2_cost>='.intval($data['search_start_f_cost']) : '';
        $searchArray[] = (isset($data['search_end_f_cost']) and !empty($data['search_end_f_cost'])) ? 'f1_cost+f2_cost<='.intval($data['search_end_f_cost']) : '';
        $searchArray[] = (isset($data['search_start_n_cost']) and !empty($data['search_start_n_cost'])) ? '(n1_cost+n2_cost)>='.intval($data['search_start_n_cost']) : '';
        $searchArray[] = (isset($data['search_end_n_cost']) and !empty($data['search_end_n_cost'])) ? '(n1_cost+n2_cost)<='.intval($data['search_end_n_cost']) : '';

        return array_filter($searchArray);
    }
}