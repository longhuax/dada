<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbdistribution\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\AbstractTableGateway;
use Dbdistribution\Model\DistributionGoods as dbshopCheckInData;

class DistributionGoodsTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_distribution_goods';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }

    /**
     * 添加分销商品
     * @param array $data
     * @return int|null
     */
    public function addDistributionGoods(array $data)
    {
        $row = $this->insert(dbshopCheckInData::addDistributionGoodsData($data));
        if($row) {
            return $this->getLastInsertValue();
        }
        return null;
    }

    /**
     * 获取分销商品信息
     * @param array $where
     * @return array|\ArrayObject|null
     */
    public function infoDistributionGoods(array $where)
    {
        $result = $this->select($where);
        if($result) {
            return $result->current();
        }
        return null;
    }

    /**
     * 分销商品列表
     * @param array $where
     * @return array|null
     */
    public function listDistributionGoods(array $where=array())
    {
        $result = $this->select(function (Select $select) use ($where) {
            $select->join(array('g' => 'dbshop_goods'), 'g.goods_id=dbshop_distribution_goods.goods_id', array('goods_item', 'goods_shop_price', 'goods_state'));
            $select->join(array('e' => 'dbshop_goods_extend'), 'e.goods_id=dbshop_distribution_goods.goods_id', array('goods_name'));
            $select->where($where);
        });

        if($result) {
            return $result->toArray();
        }
        return null;
    }

    /**
     * 删除分销商品
     * @param array $where
     * @return int
     */
    public function delDistributionGoods(array $where)
    {
        return $this->delete($where);
    }
}