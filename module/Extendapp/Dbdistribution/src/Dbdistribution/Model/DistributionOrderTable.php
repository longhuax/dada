<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbdistribution\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\AbstractTableGateway;
use Dbdistribution\Model\DistributionOrder as dbshopCheckInData;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class DistributionOrderTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_distribution_order';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }

    /**
     * 添加分销订单信息
     * @param array $data
     * @return int|null
     */
    public function addDistributionOrder(array $data)
    {
        $row = $this->insert(dbshopCheckInData::addDistributionOrderData($data));
        if($row) {
            return $this->getLastInsertValue();
        }
        return null;
    }

    /**
     * 获取分销订单信息
     * @param array $where
     * @return array|\ArrayObject|null
     */
    public function infoDistributionOrder(array $where)
    {
        $result = $this->select($where);
        if($result) {
            return $result->current();
        }
        return null;
    }

    /**
     * 更新信息
     * @param array $data
     * @param array $where
     * @return int
     */
    public function updateDistributionOrder(array $data, array $where)
    {
        return $this->update($data, $where);
    }

    /**
     * 分销订单列表
     * @param array $pageArray
     * @param array $where
     * @return Paginator
     */
    public function listDistributionOrder(array $pageArray, array $where=array())
    {
        $select = new Select(array('o'=>$this->table));

        //$select->join(array('o1'=>'dbshop_order'), 'o1.order_id=o.order_id', 'order_state');
        $select->columns(array('*', new Expression('
            (SELECT o1.order_state FROM dbshop_order AS o1 WHERE o1.order_id=o.order_id) AS order_state
        ')));

        if(!empty($where)) $select->where($where);
        $select->order('o.order_id DESC');
        //实例化分页处理
        $pageAdapter = new DbSelect($select, $this->adapter);
        $paginator   = new Paginator($pageAdapter);
        $paginator->setCurrentPageNumber($pageArray['page']);
        $paginator->setItemCountPerPage($pageArray['page_num']);

        return $paginator;
    }

    /**
     *
     * @param $where
     * @param string $type
     * @return int
     */
    public function distributionOrderCost($where, $type='one')
    {
        $row = $this->select(function (Select $select) use ($where, $type) {
            if($type == 'one') $select->columns(array(new Expression('COALESCE(SUM(one_level_user_cost),0) AS cost_total')));
            else $select->columns(array(new Expression('COALESCE(SUM(top_level_user_cost),0) AS cost_total')));
            $select->where($where);
        });
        if($row) {
            return $row->current()->cost_total;
        }
        return 0;
    }

    /**
     * 删除分销订单信息
     * @param $where
     * @return bool|null
     */
    public function delDistributionOrder($where)
    {
        $del = $this->delete($where);
        if($del) {
            return true;
        }
        return null;
    }
}