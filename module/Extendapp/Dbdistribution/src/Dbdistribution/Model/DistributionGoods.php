<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbdistribution\Model;


class DistributionGoods
{
    private static $dataArray = array();

    private static function checkData (array $data)
    {
        self::$dataArray['g_id'] = (isset($data['g_id']) and !empty($data['g_id']))
            ? intval($data['g_id'])
            : null;

        self::$dataArray['goods_id'] = (isset($data['goods_id']) and !empty($data['goods_id']))
            ? intval($data['goods_id'])
            : null;

        self::$dataArray['distribution_first'] = (isset($data['distribution_first']) and !empty($data['distribution_first']))
            ? floatval($data['distribution_first'])
            : null;

        self::$dataArray['distribution_first_type'] = (isset($data['distribution_first_type']) and !empty($data['distribution_first_type']))
            ? trim($data['distribution_first_type'])
            : null;

        self::$dataArray['distribution_two'] = (isset($data['distribution_two']) and !empty($data['distribution_two']))
            ? floatval($data['distribution_two'])
            : null;

        self::$dataArray['distribution_two_type'] = (isset($data['distribution_two_type']) and !empty($data['distribution_two_type']))
            ? trim($data['distribution_two_type'])
            : null;

        self::$dataArray = array_filter(self::$dataArray);


        return self::$dataArray;
    }

    public static function addDistributionGoodsData(array $data)
    {
        $data = self::checkData($data);
        return $data;
    }
}