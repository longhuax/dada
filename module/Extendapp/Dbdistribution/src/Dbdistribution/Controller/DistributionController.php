<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbdistribution\Controller;

use Admin\Controller\BaseController;
use Zend\Config\Reader\Ini;
use Zend\View\Model\ViewModel;

class DistributionController extends BaseController
{
    /**
     * 分销设置
     * @return array
     */
    public function indexAction()
    {
        $array = array();

        $iniRead    = new Ini();
        $configPath = DBSHOP_PATH . '/module/Extendapp/Dbdistribution/data/config.ini';

        if($this->request->isPost()) {
            $configArray    = array();
            $postArray      = $this->request->getPost()->toArray();

            $configArray['distribution_first']      = isset($postArray['distribution_first'])   ? $postArray['distribution_first']      : '';
            $configArray['distribution_first_type'] = isset($postArray['distribution_first_type'])   ? $postArray['distribution_first_type']      : '%';
            $configArray['distribution_two_top']    = isset($postArray['distribution_two_top']) ? $postArray['distribution_two_top']    : '';
            $configArray['distribution_two_top_type']= isset($postArray['distribution_two_top_type']) ? $postArray['distribution_two_top_type']    : '%';
            $configArray['distribution_state']      = isset($postArray['distribution_state'])   ? $postArray['distribution_state']      : '';
            $configArray['distribution_info']       = isset($postArray['distribution_info'])     ? htmlspecialchars($postArray['distribution_info'], ENT_QUOTES)      : '';
            $configArray['distribution_info_pc']    = isset($postArray['distribution_info_pc'])     ? htmlspecialchars($postArray['distribution_info_pc'], ENT_QUOTES)      : '';
            $configArray['distribution_info_phone'] = isset($postArray['distribution_info_phone'])  ? htmlspecialchars($postArray['distribution_info_phone'], ENT_QUOTES)   : '';

            $iniWrite = new \Zend\Config\Writer\Ini();
            $iniWrite->toFile($configPath, $configArray);

            $array['success_msg'] = $this->getDbshopLang()->translate('分销设置成功！');
        }

        if(file_exists($configPath)) {
            $array['distribution'] = $iniRead->fromFile($configPath);
        }

        return $array;
    }

    /**
     * 顶级分销用户列表
     * @return array
     */
    public function topUserListAction()
    {
        $array          = array();
        $searchArray    = array();
        if($this->request->isGet()) {
            $searchArray          = $this->request->getQuery()->toArray();
            $array['searchArray'] = $searchArray;
        }
        //会员列表
        $page = $this->params('page',1);
        $array['user_list'] = $this->getDbshopTable('DistributionUserTable')->topDistributionUserList(array('page'=>$page, 'page_num'=>20), $searchArray);
        $array['page']      = $page;

        return $array;
    }

    /**
     * 次级分销用户列表
     * @return array
     */
    public function userListAction()
    {
        $array          = array();
        $searchArray    = array();
        if($this->request->isGet()) {
            $searchArray          = $this->request->getQuery()->toArray();
            $array['searchArray'] = $searchArray;
        }
        //会员列表
        $page = $this->params('page',1);
        $array['user_list'] = $this->getDbshopTable('DistributionUserTable')->distributionUserList(array('page'=>$page, 'page_num'=>20), $searchArray);
        $array['page']      = $page;

        return $array;
    }


    public function subUserListAction()
    {
        $array = array();

        $id = (int) $this->request->getQuery('id');
        $userInfo = $this->getDbshopTable('UserTable')->infoUser(array('user_id'=>$id));
        $array['user_info'] = $userInfo;
        //会员列表
        $page = $this->params('page',1);
        $array['user_list'] = $this->getDbshopTable('DistributionUserTable')->subDistributionUserList(array('page'=>$page, 'page_num'=>20), array('u.one_level_user_id='.$id.' or u.top_level_user_id='.$id), $id);
        $array['page']      = $page;

        return $array;
    }

    public function subTopUserListAction()
    {
        $array = array();

        $id = (int) $this->request->getQuery('id');
        $userInfo = $this->getDbshopTable('UserTable')->infoUser(array('user_id'=>$id));
        $array['user_info'] = $userInfo;
        //会员列表
        $page = $this->params('page',1);
        $array['user_list'] = $this->getDbshopTable('DistributionUserTable')->subDistributionUserList(array('page'=>$page, 'page_num'=>20), array('u.one_level_user_id='.$id.' or u.top_level_user_id='.$id), $id);
        $array['page']      = $page;

        return $array;
    }

    /**
     * 分销用户订单列表
     * @return array
     */
    public function distributionUserOrderListAction()
    {
        $array = array();

        $id = (int) $this->request->getQuery('id');
        $pg = (int) $this->request->getQuery('pg');
        $state = (int) $this->request->getQuery('state', 1);
        $type = $this->request->getQuery('type');

        $typeArray = array('user'=>'userList', 'subUser'=>'subUserList', 'subTop'=>'subTopUserList', 'topUser'=>'topUserList');

        $array['pg'] = $pg;
        $array['action'] = $typeArray[$type];

        $userInfo = $this->getDbshopTable('UserTable')->infoUser(array('user_id'=>$id));
        $array['user_info'] = $userInfo;

        //会员列表
        $page = $this->params('page',1);
        $array['order_list'] = $this->getDbshopTable('DistributionOrderTable')->listDistributionOrder(array('page'=>$page, 'page_num'=>20), array('(o.one_level_user_id='.$id.' or o.top_level_user_id='.$id.') and o.o_state='.$state));
        $array['page']      = $page;

        //提成金额
        $dOneCost = $this->getDbshopTable('DistributionOrderTable')->distributionOrderCost(array('one_level_user_id'=>$id, 'o_state'=>$state));
        $dTwoCost = $this->getDbshopTable('DistributionOrderTable')->distributionOrderCost(array('top_level_user_id'=>$id, 'o_state'=>$state), 'two');
        $array['d_cost'] = $dOneCost + $dTwoCost;

        return $array;
    }

    /**
     * 下级分销订单
     * @return array
     */
    public function subDistributionUserOrderListAction()
    {
        $array = array();

        $id = (int) $this->request->getQuery('id');
        $dId = (int) $this->request->getQuery('d_id');
        $pg = (int) $this->request->getQuery('pg');
        $state = (int) $this->request->getQuery('state', 1);
        $type = $this->request->getQuery('type');

        $typeArray = array('subUser'=>'subUserList', 'subTop'=>'subTopUserList');

        $array['d_id'] = $dId;
        $array['pg'] = $pg;
        $array['action'] = $typeArray[$type];

        $userInfo = $this->getDbshopTable('UserTable')->infoUser(array('user_id'=>$id));
        $array['user_info'] = $userInfo;

        //会员列表
        $page = $this->params('page',1);
        $array['order_list'] = $this->getDbshopTable('DistributionOrderTable')->listDistributionOrder(array('page'=>$page, 'page_num'=>20), array('(o.one_level_user_id='.$dId.' or o.top_level_user_id='.$dId.') and o.buy_user_id='.$id.' and o.o_state='.$state));
        $array['page']      = $page;

        //提成金额
        $dOneCost = $this->getDbshopTable('DistributionOrderTable')->distributionOrderCost(array('one_level_user_id'=>$dId, 'buy_user_id'=>$id, 'o_state'=>$state));
        $dTwoCost = $this->getDbshopTable('DistributionOrderTable')->distributionOrderCost(array('top_level_user_id'=>$dId, 'buy_user_id'=>$id, 'o_state'=>$state), 'two');
        $array['d_cost'] = $dOneCost + $dTwoCost;

        return $array;
    }

    /**
     * 分销商品设置
     * @return array
     */
    public function specDistributionGoodsAction()
    {
        $distributionGoods = $this->getDbshopTable('DistributionGoodsTable')->listDistributionGoods();

        return array('goods_list'=> $distributionGoods);
    }

    /**
     * 添加分销商品
     */
    public function addDistributionGoodsAction()
    {
        $goodsId                = (int) $this->request->getPost('goods_id', 0);
        $distributionFirst      = floatval($this->request->getPost('distribution_first', 0));
        $distributionFirstType  = $this->request->getPost('distribution_first_type');
        $distributionTwo        = floatval($this->request->getPost('distribution_two', 0));
        $distributionTwoType    = $this->request->getPost('distribution_two_type');

        $distributionGoods = $this->getDbshopTable('DistributionGoodsTable')->infoDistributionGoods(array('goods_id'=>$goodsId));
        if($distributionGoods) exit(json_encode(array('state'=>'have')));

        $goodsInfo = $this->getDbshopTable('GoodsTable')->oneGoodsInfo(array('goods_id'=>$goodsId));
        if($goodsInfo && $distributionFirst >=0 && $distributionTwo >= 0 && in_array($distributionFirstType, array('%', '$')) && in_array($distributionTwoType, array('%', '$'))) {
            $addDistributionGoods = array(
                'goods_id'                  => $goodsId,
                'distribution_first'        => $distributionFirst,
                'distribution_first_type'   => $distributionFirstType,
                'distribution_two'          => $distributionTwo,
                'distribution_two_type'     => $distributionTwoType
            );
            $this->getDbshopTable('DistributionGoodsTable')->addDistributionGoods($addDistributionGoods);
            exit(json_encode(array('state'=>'true')));
        }
        exit(json_encode(array('state'=>'false')));
    }

    /**
     * 删除分销商品
     */
    public function delDistributionGoodsAction()
    {
        $gId = (int) $this->request->getPost('g_id', 0);
        $distributionGoods = $this->getDbshopTable('DistributionGoodsTable')->infoDistributionGoods(array('g_id'=>$gId));
        if(!$distributionGoods) exit('false');

        $this->getDbshopTable('DistributionGoodsTable')->delDistributionGoods(array('g_id'=>$gId));
        exit('true');
    }

    /**
     * 数据表调用
     * @param string $tableName
     * @return multitype:
     */
    private function getDbshopTable ($tableName)
    {
        if (empty($this->dbTables[$tableName])) {
            $this->dbTables[$tableName] = $this->getServiceLocator()->get($tableName);
        }
        return $this->dbTables[$tableName];
    }
}