<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbdistribution\Controller;

use Shopfront\Controller\FronthomeController;
use Zend\Config\Reader\Ini;
use Zend\View\Model\ViewModel;

class IndexController extends FronthomeController
{
    private $dbTables = array();
    private $translator;

    public function indexAction()
    {
        //顶部title使用
        $this->layout()->title_name = $this->getDbshopLang()->translate('我的分销&收入');

        $view = new ViewModel();
        $view->setTemplate('/shopfront/home/home-right-empty.phtml');

        $array  = array();
        $array['partial_template_file'] = '/dbdistribution/index/index.phtml';
        $array['title_name'] = $this->getDbshopLang()->translate('我的分销&收入');

        $userId = $this->getServiceLocator()->get('frontHelper')->getUserSession('user_id');

        $iniRead= new Ini();
        $config = $iniRead->fromFile(DBSHOP_PATH . '/module/Extendapp/Dbdistribution/data/config.ini');
        $array['info'] = $config['distribution_info_pc'];

        //分销收入
        $fOneCost = $this->getDbshopTable('DistributionOrderTable')->distributionOrderCost(array('one_level_user_id'=>$userId, 'o_state'=>1));
        $nOneCost = $this->getDbshopTable('DistributionOrderTable')->distributionOrderCost(array('one_level_user_id'=>$userId, 'o_state'=>2));
        $fTopCost = $this->getDbshopTable('DistributionOrderTable')->distributionOrderCost(array('top_level_user_id'=>$userId, 'o_state'=>1), 'top');
        $nTopCost = $this->getDbshopTable('DistributionOrderTable')->distributionOrderCost(array('top_level_user_id'=>$userId, 'o_state'=>2), 'top');
        $array['f_cost'] = $fOneCost + $fTopCost;
        $array['n_cost'] = $nOneCost + $nTopCost;

        $page = $this->params('page',1);
        $array['user_list'] = $this->getDbshopTable('DistributionUserTable')->subDistributionUserList(array('page'=>$page, 'page_num'=>20), array('u.one_level_user_id='.$userId.' or u.top_level_user_id='.$userId), $userId);
        $array['page']      = $page;

        $view->setVariables($array);
        return $view;
    }

    /**
     * 数据表调用
     * @param string $tableName
     * @return multitype:
     */
    private function getDbshopTable ($tableName)
    {
        if (empty($this->dbTables[$tableName])) {
            $this->dbTables[$tableName] = $this->getServiceLocator()->get($tableName);
        }
        return $this->dbTables[$tableName];
    }
    /**
     * 语言包调用
     * @return array|object
     */
    private function getDbshopLang ()
    {
        if (! $this->translator) {
            $this->translator = $this->getServiceLocator()->get('translator');
        }
        return $this->translator;
    }
}