DROP TABLE IF EXISTS dbshop_distribution_user;
CREATE TABLE `dbshop_distribution_user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_name` char(100) NOT NULL,
  `one_level_user_id` int(11) NOT NULL,
  `one_level_user_name` char(100) NOT NULL,
  `top_level_user_id` int(11) DEFAULT '0',
  `top_level_user_name` char(100) DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  KEY `user_id` (`user_id`,`one_level_user_id`,`top_level_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分销用户表';

DROP TABLE IF EXISTS dbshop_distribution_order;
CREATE TABLE `dbshop_distribution_order` (
  `o_id` int(11) NOT NULL AUTO_INCREMENT,
  `o_state` tinyint(1) NOT NULL DEFAULT '2',
  `o_finish_time` int(10) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `order_sn` char(50) NOT NULL,
  `buy_user_id` int(11) NOT NULL,
  `buy_user_name` char(100) NOT NULL,
  `one_level_user_id` int(11) NOT NULL,
  `one_level_user_name` char(100) NOT NULL,
  `one_level_user_cost` float(10,2) NOT NULL,
  `top_level_user_id` int(11) DEFAULT '0',
  `top_level_user_name` char(100) DEFAULT NULL,
  `top_level_user_cost` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`o_id`),
  KEY `o_state` (`o_state`,`order_id`,`buy_user_id`,`one_level_user_id`,`top_level_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分销提成数据表';

DROP TABLE IF EXISTS dbshop_distribution_goods;
CREATE TABLE `dbshop_distribution_goods` (
  `g_id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) NOT NULL,
  `distribution_first` float NOT NULL DEFAULT '0',
  `distribution_first_type` char(4) NOT NULL,
  `distribution_two` float NOT NULL DEFAULT '0',
  `distribution_two_type` char(4) NOT NULL,
  PRIMARY KEY (`g_id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;