<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */
return array(
    'Dbdistribution\Module'                        => __DIR__ . '/Module.php',
    'Dbdistribution\Controller\IndexController'    => __DIR__ . '/src/Dbdistribution/Controller/IndexController.php',
    'Dbdistribution\Controller\DistributionController'=> __DIR__ . '/src/Dbdistribution/Controller/DistributionController.php',

    'Dbdistribution\Model\DistributionUser'        => __DIR__ . '/src/Dbdistribution/Model/DistributionUser.php',
    'Dbdistribution\Model\DistributionUserTable'   => __DIR__ . '/src/Dbdistribution/Model/DistributionUserTable.php',
    'Dbdistribution\Model\DistributionOrder'       => __DIR__ . '/src/Dbdistribution/Model/DistributionOrder.php',
    'Dbdistribution\Model\DistributionOrderTable'  => __DIR__ . '/src/Dbdistribution/Model/DistributionOrderTable.php',
    'Dbdistribution\Model\DistributionGoods'       => __DIR__ . '/src/Dbdistribution/Model/DistributionGoods.php',
    'Dbdistribution\Model\DistributionGoodsTable'  => __DIR__ . '/src/Dbdistribution/Model/DistributionGoodsTable.php',

);
