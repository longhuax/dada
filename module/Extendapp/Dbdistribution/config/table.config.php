<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

return array(
    'DistributionUserTable'     => function  () { return new \Dbdistribution\Model\DistributionUserTable();  },
    'DistributionOrderTable'    => function  () { return new \Dbdistribution\Model\DistributionOrderTable();  },
    'DistributionGoodsTable'    => function  () { return new \Dbdistribution\Model\DistributionGoodsTable();  },
);