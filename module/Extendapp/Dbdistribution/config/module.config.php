<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Dbdistribution\Controller\Distribution'       => 'Dbdistribution\Controller\DistributionController',
            'Dbdistribution\Controller\Index'    => 'Dbdistribution\Controller\IndexController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'distribution'    => include __DIR__ . '/router/router.config.php',
            'front-distribution' => include __DIR__ . '/router/front.config.php',
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo'
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Dbdistribution' => __DIR__ . '/../view',
        )
    ),
);
