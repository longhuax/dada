<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbgoodsimport\Controller;

use Admin\Controller\BaseController;
use Upload\Common\Service\ImageUpload;
use Zend\Config\Reader\Ini;

class IndexController extends BaseController
{

    /**
     * 商品导入首页
     * @return array
     */
    public function indexAction()
    {
        $array = array();

        //商品分类
        $array['goods_class'] = $this->getDbshopTable('GoodsClassTable')->classOptions(0,$this->getDbshopTable('GoodsClassTable')->listGoodsClass());

        return $array;
    }
    /**
     * 商品导入操作
     */
    public function importGoodsAction()
    {
        @set_time_limit(500);
        $message    = '';
        $postArray  = $this->request->getPost()->toArray();
        $excelFile  = $_FILES['excel']['tmp_name'];

        if(!empty($postArray) and !empty($excelFile)) {
            if(!empty($postArray['goods_class'])) {
                $excelGoodsClassIdState = false;
                if($postArray['goods_class'] == -1) {
                    $excelGoodsClassIdState = true;
                } else {
                    $classInfo = $this->getDbshopTable('GoodsClassTable')->infoGoodsClass(array('class_id'=>$postArray['goods_class']));
                    if(empty($classInfo)) exit($this->getDbshopLang()->translate('该分类不存在，请重新选择！'). '&nbsp;&nbsp;<a href="'.$this->url()->fromRoute('dbgoodsimport/default').'">'.$this->getDbshopLang()->translate('返回').'</a>');
                    $classIdArray = explode(',', $classInfo->class_path);
                    $goodsInClassArray = $this->getGoodsInClassState($classIdArray);
                }

                $language       = $this->getDbshopLang()->getLocale();
                $colorArray     = $this->colorArray();
                $sizeArray      = $this->sizeArray();
                $uploadClass    = new ImageUpload();
                $configReader   = new Ini();
                $watermarkConfig= $configReader->fromFile(DBSHOP_PATH . '/data/moduledata/Upload/Watermark.ini');
                if(!empty($watermarkConfig['image']['watermark_image'])) $watermarkConfig['image']['watermark_image'] = DBSHOP_PATH . $watermarkConfig['image']['watermark_image'];
                $waterOption = array_merge($watermarkConfig['config'], $watermarkConfig['image'], $watermarkConfig['text']);

                $uploadConfig   = $configReader->fromFile(DBSHOP_PATH . '/data/moduledata/Upload/Goods.ini');
                $savePath       = '/public/upload/goods/' . date("Ymd") . '/';
                //检测图片存放目录，设定存放目录
                if(!is_dir(DBSHOP_PATH . $savePath)) {
                    mkdir(rtrim(DBSHOP_PATH . $savePath,'/'),0755, true);
                    chmod(DBSHOP_PATH . $savePath, 0755);
                }

                $imageRelativePath  = $_SERVER['PHP_SELF'] ? dirname($_SERVER['PHP_SELF']) : dirname($_SERVER['SCRIPT_NAME']);
                $imageRelativePath .= $savePath;
                $imageRelativePath  = str_replace('//', '/', $imageRelativePath);

                require_once DBSHOP_PATH . '/module/Upload/src/Upload/Plugin/Phpexcel/PHPExcel/Reader/Excel2007.php';
                $excelReader    = new \PHPExcel_Reader_Excel2007();
                $objPhpexcel    = $excelReader->load($excelFile);
                $currentSheet   = $objPhpexcel->getSheet(0);
                $lineNum        = $currentSheet->getHighestRow();
                for($num = 2; $num <= $lineNum; $num++) {
                    $goodsBody  = $currentSheet->getCell('O'.$num)->getValue();//商品详情,之所以放这里，是为下面图片替换做准备

                    $goodsName  = $currentSheet->getCell('A'.$num)->getValue(); //商品名称
                    if(empty($goodsName)) continue;

                    $goodsPrice = $currentSheet->getCell('B'.$num)->getValue(); //商品价格
                    $goodsType  = $currentSheet->getCell('C'.$num)->getValue(); //商品类型
                    $goodsType  = (!empty($goodsType) and in_array($goodsType, array(1,2))) ? $goodsType : 1;
                    $goodsState = $currentSheet->getCell('D'.$num)->getValue(); //商品状态
                    $goodsState = (!empty($goodsState) and in_array($goodsState, array(1,2))) ? $goodsState : 1;

                    $goodsItem      = $currentSheet->getCell('J'.$num)->getValue();//商品货号
                    $goodsItem      = empty($goodsItem) ? $this->getDbshopTable('GoodsTable')->autoCreateGoodsItem($this->getServiceLocator()->get('frontHelper')->getDbshopGoodsIni('dbshop_goods_sn_prefix')) : $goodsItem;
                    $price          = $currentSheet->getCell('K'.$num)->getValue();//市场价格
                    $brandId        = $currentSheet->getCell('L'.$num)->getValue();//商品品牌
                    $virtualSales   = $currentSheet->getCell('M'.$num)->getValue();//虚拟销量
                    $goodsWeight    = $currentSheet->getCell('N'.$num)->getValue();//商品重量

                    //商品分类
                    $goodsClassId    = $currentSheet->getCell('P'.$num)->getValue();//商品分类ID
                    if($excelGoodsClassIdState and $goodsClassId > 0) {
                        $classInfo = $this->getDbshopTable('GoodsClassTable')->infoGoodsClass(array('class_id'=>$goodsClassId));
                        $goodsInClassArray = array();
                        if(!empty($classInfo)) {
                            $classIdArray = explode(',', $classInfo->class_path);
                            $goodsInClassArray = $this->getGoodsInClassState($classIdArray);
                        }
                    }
                    //商品基础信息
                    $goodsArray = array(
                        'goods_shop_price'          => $goodsPrice,
                        'goods_type'                => $goodsType,
                        'goods_state'               => $goodsState,
                        'goods_stock_state'         => 1,
                        'goods_out_stock_state'     => 2,
                        'goods_stock'               => 10,
                        'goods_out_of_stock_set'    => 1,
                        'goods_class_have_true'     => 1,

                        'goods_item'    => $goodsItem,
                        'goods_price'   => $price,
                        'brand_id'      => $brandId,
                        'virtual_sales' => $virtualSales,
                        'goods_weight'  => $goodsWeight,
                        'goods_stock_state_open' => 1
                    );
                    //设置主分类
                    if($excelGoodsClassIdState and !empty($goodsInClassArray)) $goodsArray['main_class_id'] = $goodsClassId;
                    else $goodsArray['main_class_id'] = $postArray['goods_class'];
                    //商品基本信息
                    $goodsId    = $this->getDbshopTable('GoodsTable')->addGoods($goodsArray);

                    //商品分类
                    if(!empty($goodsInClassArray)) $this->getDbshopTable('GoodsInClassTable')->addGoodsInClass($goodsId,$goodsInClassArray);

                    //商品图片
                    $imageStr   = $currentSheet->getCell('E'.$num)->getValue();
                    $defaultImage = '';
                    if(!empty($imageStr)) {
                        $imageStr   = str_replace("\r", '', $imageStr);
                        $imageStr   = str_replace("\n", '', $imageStr);
                        $imageStr   = str_replace('，', ',', $imageStr);
                        if(!empty($imageStr)) {
                            $imageArray = explode(',', $imageStr);
                            $imagePath  = DBSHOP_PATH . '/public/upload/importimage/';
                            $imageI = 1;
                            foreach($imageArray as $imageValue) {
                                $oneImage       = explode('|', $imageValue);
                                $imageValue     = $oneImage[0];
                                $imageSlideState= (isset($oneImage[1]) and $oneImage[1] == 0) ? 2 : 1;
                                $sourceImage    = $imagePath . $imageValue;
                                $toImage        = DBSHOP_PATH . $savePath . $imageValue;
                                if(file_exists($sourceImage)) {
                                    if(copy($sourceImage, $toImage)) {
                                        if($uploadConfig['goods']['goods_watermark_state'] == 1 and $watermarkConfig['config']['watermark_state'] != 'close') {
                                            \WaterMark::output($toImage, $toImage, $waterOption);
                                        }
                                        $thumpImage = $uploadClass->createThumbImage($savePath, $imageValue, array(
                                            'width' => $uploadConfig['goods']['goods_thumb_width'],
                                            'height'=> $uploadConfig['goods']['goods_thumb_heigth'],
                                            'crop'  => $uploadConfig['goods']['goods_image_crop'],
                                            'rename'=> true
                                        ));
                                        $imageId = $this->getDbshopTable('GoodsImageTable')->addImage(array(
                                            'goods_title_image'=> $savePath . $imageValue,
                                            'goods_thumbnail_image'=> $thumpImage,
                                            'goods_watermark_image'=> $savePath . $imageValue,
                                            'goods_source_image'=> $savePath . $imageValue,
                                            'image_slide'=> $imageSlideState,
                                            'image_sort'=> 255,
                                            'language'=> $language,
                                            'goods_id'=> $goodsId
                                        ));

                                        $editorImage = '<p><img src="'.$imageRelativePath.$imageValue.'" /></p>';
                                        if(!empty($goodsBody)) $goodsBody = str_replace($imageValue, $editorImage, $goodsBody);

                                        if($imageSlideState == 1 and empty($defaultImage)) $defaultImage = $imageId;

                                        $imageI++;
                                    }
                                }
                            }

                        }
                    }

                    if(!empty($goodsBody)) $goodsBody = str_replace(array("\n", "\r"), array('<br>', '<br>'), $goodsBody);
                    //商品扩展信息
                    $goodsExtendName = $currentSheet->getCell('I'.$num)->getValue();//商品扩展名称
                    $this->getDbshopTable('GoodsExtendTable')->addGoodsExtend(array(
                        'goods_id'          => $goodsId,
                        'goods_name'        => $goodsName,
                        'language'          => $language,
                        'goods_extend_name' => $goodsExtendName,
                        'goods_body'        => $goodsBody,
                        'default_image'     => (isset($defaultImage) ? $defaultImage : '')
                    ));
                    //商品规格-颜色
                    $colorValueArray = array();
                    $goodsColorStr = $currentSheet->getCell('F'.$num)->getValue();
                    if(!empty($goodsColorStr)) {
                        $goodsColorStr = str_replace('：', ':', $goodsColorStr);
                        $goodsColorStr = str_replace('，', ',', $goodsColorStr);
                        $extendColorArray = explode(':', $goodsColorStr);
                        $extendId = $this->getDbshopTable('GoodsPriceExtendTable')->editPriceExtend(
                            array(
                                'extend_name'   => $extendColorArray[0],
                                'goods_id'      => $goodsId,
                                'extend_type'   => 'one',
                                'extend_show_type'=> 1,
                                'language'      => $language
                            ), array('goods_id'=>$goodsId, 'extend_type'=>'one')
                        );
                        if(isset($extendColorArray[1]) and !empty($extendColorArray[1])) {
                            $this->getDbshopTable('GoodsPriceExtendColorTable')->delGoodsPriceExtendColor(array('goods_id'=>$goodsId,'extend_id'=>$extendId));
                            $colorValueArray = explode(',', $extendColorArray[1]);
                            foreach($colorValueArray as $colorKey => $colorValue) {
                                $this->getDbshopTable('GoodsPriceExtendColorTable')->addGoodsPriceExtendColor(array(
                                    'color_value'   => ($colorValue == 'null' ? 'kongzhi' : $colorArray[$colorKey]),
                                    'color_info'    => ($colorValue == 'null' ? 'kongzhi' : $colorValue),
                                    'extend_id'     => $extendId,
                                    'goods_id'      => $goodsId
                                ));
                            }
                        }
                    }

                    //商品规格-尺寸
                    $sizeValueArray = array();
                    $goodsSizeStr = $currentSheet->getCell('G'.$num)->getValue();
                    if(!empty($goodsSizeStr)) {
                        $goodsSizeStr = str_replace('：', ':', $goodsSizeStr);
                        $goodsSizeStr = str_replace('，', ',', $goodsSizeStr);
                        $extendSizeArray = explode(':', $goodsSizeStr);
                        $extendId = $this->getDbshopTable('GoodsPriceExtendTable')->editPriceExtend(
                            array(
                                'extend_name'   => $extendSizeArray[0],
                                'goods_id'      => $goodsId,
                                'extend_type'   => 'two',
                                'extend_show_type'=> 1,
                                'language'      => $language
                            ),array('goods_id'=>$goodsId,'extend_type'=>'two')
                        );
                        if(isset($extendSizeArray[1]) and !empty($extendSizeArray[1])) {
                            $this->getDbshopTable('GoodsPriceExtendSizeTable')->delPriceExtendSize(array('goods_id'=>$goodsId,'extend_id'=>$extendId));
                            $sizeValueArray = explode(',', $extendSizeArray[1]);
                            foreach($sizeValueArray as $sizeKey => $sizeValue) {
                                $this->getDbshopTable('GoodsPriceExtendSizeTable')->addPriceExtendSize(array(
                                    'size_value'    => $sizeArray[$sizeKey],
                                    'size_info'     => $sizeValue,
                                    'extend_id'     => $extendId,
                                    'goods_id'      => $goodsId
                                ));
                            }
                        }
                    }
                    //颜色和规格都存在时
                    if(isset($colorValueArray) and !empty($colorValueArray) and isset($sizeValueArray) and !empty($sizeValueArray)) {
                        $colorSizePriceStr = $currentSheet->getCell('H'.$num)->getValue();
                        $cZArray = array();
                        if(!empty($colorSizePriceStr)) {
                            $colorSizePriceStr = str_replace('，', ',', $colorSizePriceStr);
                            $colorSizePriceStr = str_replace('：', ':', $colorSizePriceStr);
                            $colorSizeArray = explode(',', $colorSizePriceStr);
                            foreach($colorSizeArray as $colorSizeValue) {
                                $cArray = explode(':', $colorSizeValue);
                                if(isset($cArray[0]) and isset($cArray[1])) $cZArray[$cArray[0]] = $cArray[1];
                            }
                        }

                        $i = 1;
                        foreach($colorValueArray as $cKey => $cValue) {
                            foreach($sizeValueArray as $sKey => $sValue) {
                                $cZstr = ($cValue == 'null' ? $sValue : $cValue.'|'.$sValue);
                                if(!empty($cZArray) and isset($cZArray[$cZstr]) and $cZArray[$cZstr] > 0) $goodsPrice = $cZArray[$cZstr];

                                $this->getDbshopTable('GoodsPriceExtendGoodsTable')->addPriceExtendGoods(array(
                                    'goods_color'=> ($cValue == 'null' ? 'kongzhi' : $colorArray[$cKey]),
                                    'goods_size'=> $sizeArray[$sKey],
                                    'goods_extend_price'=>$goodsPrice,
                                    'goods_extend_stock'=> 10,
                                    'goods_id'=> $goodsId,
                                    'goods_extend_item' => $goodsItem . '-' .$i
                                ));
                                $i++;
                            }
                        }
                    }
                }
                $message = $this->getDbshopLang()->translate('商品批量导入完成');
            } else $message = $this->getDbshopLang()->translate('商品分类不存在');
        }
        exit($message . '&nbsp;&nbsp;<a href="'.$this->url()->fromRoute('dbgoodsimport/default').'">'.$this->getDbshopLang()->translate('返回').'</a>');
    }
    /**
     * 批量导入例子文档下载
     */
    public function exampleExcelAction()
    {
        $downloadFile = DBSHOP_PATH . '/module/Extendapp/Dbgoodsimport/data/example.xlsx';
        $fileName     = basename($downloadFile);

        $file = fopen($downloadFile,"r");
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Accept-Length: ".filesize($downloadFile));
        Header("Content-Disposition: attachment; filename=".$fileName);
        echo fread($file, filesize($downloadFile));
        fclose($file);
        exit;
    }
    private function colorArray()
    {
        return array(
            0 => '5d762a',
            1 => '1eddff',
            2 => 'd2691e',
            3 => 'ffa500',
            4 => 'e4e4e4',

            5 => '98fb98',
            6 => 'ffffb1',
            7 => 'bdb76b',
            8 => '666666',
            9 => '4b0082',

            10 => '041690',
            11 => 'ffffff',
            12 => 'ffb6c1',
            13 => 'dda0dd',
            14 => '800080',

            15 => 'ff0000',
            16 => '008000',
            17 => '0000ff',
            18 => '855b00',
            19 => '990000',

            20 => 'ffff00',
            21 => '000000'
        );
    }
    private function sizeArray()
    {
        return array(
            0 => 'XXS',
            1 => 'XS',
            2 => 'S',
            3 => 'M',
            4 => 'L',

            5 => 'XL',
            6 => 'XXL',
            7 => 'XXXL',
            8 => 'XXXXL',
            9 => 'jm',

            10 => 'other1',
            11 => 'other2',
            12 => 'other3',
            13 => 'other4',
            14 => 'other5',

            15 => 'other6',
            16 => 'other7',
            17 => 'other8',
            18 => 'other9',
            19 => 'other10'
        );
    }
    /**
     * 获取将要插入商品扩展分类表提取分类状态
     * @param array $classIdArray
     * @return multitype:unknown
     */
    private function getGoodsInClassState(array $classIdArray=array())
    {
        $classStateArray = array();
        if(is_array($classIdArray) and !empty($classIdArray)) {
            $classList = $this->getDbshopTable('GoodsClassTable')->selectGoodsClass('class_id IN ('. implode(',', $classIdArray) .')');
            foreach ($classList as $value) {
                $classStateArray[$value['class_id']] = $value['class_state'];
            }
        }
        return $classStateArray;
    }
    /**
     * 数据表调用
     * @param string $tableName
     * @return multitype:
     */
    private function getDbshopTable ($tableName)
    {
        if (empty($this->dbTables[$tableName])) {
            $this->dbTables[$tableName] = $this->getServiceLocator()->get($tableName);
        }
        return $this->dbTables[$tableName];
    }
}