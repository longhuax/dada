<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */
return array(
    'Dbgoodsimage\Module'                        => __DIR__ . '/Module.php',
    'Dbgoodsimage\Controller\IndexController'    => __DIR__ . '/src/Dbgoodsimage/Controller/IndexController.php',
);
