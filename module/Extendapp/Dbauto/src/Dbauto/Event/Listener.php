<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbauto\Event;

use Zend\Config\Reader\Ini;
use Zend\EventManager\Event;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

class Listener implements ListenerAggregateInterface
{
    protected $listeners = array();

    private $autoConfig;
    private $autoOrderConfig;
    private $autoOtherConfig;

    public function __construct()
    {
        $iniRead = new Ini();
        if(empty($this->autoOrderConfig)) {
            $this->autoOrderConfig = $iniRead->fromFile(DBSHOP_PATH . '/module/Extendapp/Dbauto/data/auto/order.ini');
        }
        if(empty($this->autoConfig)) {
            $this->autoConfig = $iniRead->fromFile(DBSHOP_PATH . '/module/Extendapp/Dbauto/data/config.ini');
        }
        if(empty($this->autoOtherConfig)) {
            $this->autoOtherConfig = $iniRead->fromFile(DBSHOP_PATH . '/module/Extendapp/Dbauto/data/auto/otherauto.ini');
        }
    }
    /**
     * @param EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events)
    {
        $shareEvents = $events->getSharedManager();

        //后台
        $this->listeners[] = $shareEvents->attach(//删除订单事件
            'Orders\Controller\OrdersController',
            'order.del.backstage.post',
            array($this, 'onAdminDelAllAutoOrder')
        );
        $this->listeners[] = $shareEvents->attach(//订单发货事件
            array('Orders\Controller\OrdersController'),
            'order.deliver.backstage.post',
            array($this, 'onSaveFinishAutoOrder')
        );
        $this->listeners[] = $shareEvents->attach(//订单确认事件
            array(
                'Orders\Controller\OrdersController',
                'Dbauto\Controller\AutoController'
            ),
            'order.finish.backstage.post',
            array($this, 'onSaveEvaluateAutoOrder')
        );
        /*-----------------------由于是独立插件，下面的事件驱动为了不影响其他模块，所以在插件内直接完成---------------------------*/
        $this->listeners[] = $shareEvents->attach('Dbauto\Controller\AutoController', 'order.finish.backstage.post',
            array($this, 'onUpdateUserCoupon')
        );
        /*-----------------------由于是独立插件，上面的事件驱动为了不影响其他模块，所以在插件内直接完成---------------------------*/

        $this->listeners[] = $shareEvents->attach(//订单确认事件,添加分销商分成处理
            array(
                'Orders\Controller\OrdersController',
                'Dbauto\Controller\AutoController'
            ),
            'order.finish.backstage.post',
            array($this, 'onSaveDistributionAutoOrder')
        );
        $this->listeners[] = $shareEvents->attach(//pc端、phone端、手机api端 订单确认收货事件,添加分销商分成处理
            array(
                'Shopfront\Controller\OrderController',
                'Mobile\Controller\OrderController',
                'Dbapi\Controller\JsonapiController'
            ),
            'order.finish.front.post',
            array($this, 'onSaveDistributionAutoOrder')
        );

        //前台
        $this->listeners[] = $shareEvents->attach(//pc端、phone端、手机api端 提交订单事件
            array(
                'Shopfront\Controller\CartController',
                'Mobile\Controller\CartController',
                'Dbapi\Controller\JsonapiController',
            ),
            'cart.submit.front.post',
            array($this, 'onSaveAutoOrder')
        );
        $this->listeners[] = $shareEvents->attach(//pc端、phone端、手机api端 删除订单事件
            array(
                'Shopfront\Controller\OrderController',
                'Mobile\Controller\OrderController',
                'Dbapi\Controller\JsonapiController',
            ),
            'order.del.front.post',
            array($this, 'onDelAutoOrder')
        );
        $this->listeners[] = $shareEvents->attach(//pc端、phone端、手机api端 订单确认收货事件
            array(
                'Shopfront\Controller\OrderController',
                'Mobile\Controller\OrderController',
                'Dbapi\Controller\JsonapiController'
            ),
            'order.finish.front.post',
            array($this, 'onSaveEvaluateAutoOrder')
        );
    }
    /**
     * @param EventManagerInterface $events
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    /**
     * 后台订单发货，进行确认收货的自动处理信息添加（不处理货到付款的情况）
     * @param Event $e
     */
    public function onSaveFinishAutoOrder(Event $e)
    {
        $orderId = $e->getParam('values');
        if(
            //$values['order_info']->pay_code != 'hdfk' and
            $this->autoConfig['dbshopAutoState'] == 1
            and $this->autoOrderConfig['receipt']['state'] == 1
            and $this->autoOrderConfig['receipt']['time'] > 0
            and is_numeric($orderId) and $orderId > 0
        ){
            $orderTable = $e->getTarget()->getServiceLocator()->get('OrderTable');
            $orderInfo  = $orderTable->infoOrder(array('order_id'=>$orderId));

            $autoOrder = array();
            $autoOrder['order_id']      = $orderId;
            $autoOrder['user_id']       = $orderInfo->buyer_id;
            $autoOrder['user_name']     = $orderInfo->buyer_name;
            $autoOrder['order_state']   = 40;
            $autoOrder['order_change_state'] = 60;
            $autoOrder['change_time']   = time() + $this->autoOrderConfig['receipt']['time'];

            $autoTable = $e->getTarget()->getServiceLocator()->get('DbautoOrderTable');
            $autoTable->addAutoOrder($autoOrder);
            //删除掉取消的自动处理
            $autoTable->delAutoOrder(array('order_id'=>$autoOrder['order_id'], 'order_state'=>10));
        }
    }
    /**
     * 后台批量删除订单时，对自动信息进行删除
     * @param Event $e
     */
    public function onAdminDelAllAutoOrder(Event $e)
    {
        $values = $e->getParam('values');
        if(is_array($values) and !empty($values)) {
            $autoTable = $e->getTarget()->getServiceLocator()->get('DbautoOrderTable');
            $autoTable->delAutoOrder(array('order_id'=>$values));
        }
    }
    /**
     * 前台提交订单时，对自动信息进行保存（不处理货到付款的情况）
     * @param Event $e
     */
    public function onSaveAutoOrder(Event $e)
    {
        $orderId = $e->getParam('order_id');
        $paymentCode = $e->getParam('payment_code');
        if(
            $paymentCode != 'hdfk' and
            $this->autoConfig['dbshopAutoState'] == 1
            and $this->autoOrderConfig['cancel']['state'] == 1
            and $this->autoOrderConfig['cancel']['time'] > 0
            and is_numeric($orderId) and $orderId > 0
        ) {
            $orderTable = $e->getTarget()->getServiceLocator()->get('OrderTable');
            $orderInfo  = $orderTable->infoOrder(array('order_id'=>$orderId));

            $autoOrder = array();
            $autoOrder['order_id']      = $orderId;
            $autoOrder['user_id']       = $orderInfo->buyer_id;
            $autoOrder['user_name']     = $orderInfo->buyer_name;
            $autoOrder['order_state']   = 10;
            $autoOrder['order_change_state'] = 0;
            $autoOrder['change_time']   = time() + $this->autoOrderConfig['cancel']['time'];
            $autoTable = $e->getTarget()->getServiceLocator()->get('DbautoOrderTable');
            $autoTable->addAutoOrder($autoOrder);
        }
    }
    /**
     * 订单确认收货（前后台），进行评价的自动设置信息添加
     * @param Event $e
     */
    public function onSaveEvaluateAutoOrder(Event $e)
    {
        $orderId = $e->getParam('values');
        if(
            $this->autoOrderConfig['evaluate']['state'] == 1
            and $this->autoConfig['dbshopAutoState'] == 1
            and $this->autoOrderConfig['evaluate']['time'] > 0
            and is_numeric($orderId) and $orderId > 0
        ) {
            $orderTable = $e->getTarget()->getServiceLocator()->get('OrderTable');
            $orderInfo  = $orderTable->infoOrder(array('order_id'=>$orderId));

            $autoOrder = array();
            $autoOrder['order_id']      = $orderInfo->order_id;
            $autoOrder['user_id']       = $orderInfo->buyer_id;
            $autoOrder['user_name']     = $orderInfo->buyer_name;
            $autoOrder['order_state']   = 60;
            $autoOrder['order_change_state'] = 100;//将100定为评价处理
            $autoOrder['change_time']   = time() + $this->autoOrderConfig['evaluate']['time'];

            $autoTable = $e->getTarget()->getServiceLocator()->get('DbautoOrderTable');
            $autoTable->addAutoOrder($autoOrder);
            //删除掉取消的自动处理
            $autoTable->delAutoOrder(array('order_id'=>$autoOrder['order_id'], 'order_state'=>10));
            //删除掉确认收货的自动处理
            $autoTable->delAutoOrder(array('order_id'=>$autoOrder['order_id'], 'order_state'=>40));

        }
    }
    /**
     * 前台删除订单时，对自动的处理信息进行删除
     * @param Event $e
     */
    public function onDelAutoOrder(Event $e)
    {
        $values = $e->getParam('values');
        if(isset($values->order_id) and $values->order_id > 0) {
            $autoTable = $e->getTarget()->getServiceLocator()->get('DbautoOrderTable');
            $autoTable->delAutoOrder(array('order_id'=>$values->order_id));
        }
    }

    /**
     * 订单完成时，设置提成发放时间
     * @param Event $e
     */
    public function onSaveDistributionAutoOrder(Event $e)
    {
        $orderId = $e->getParam('values');
        if(is_numeric($orderId) and $orderId > 0) {
            if(isset($GLOBALS['extendModule']['modules']) and !empty($GLOBALS['extendModule']['modules']) and in_array('Dbdistribution', $GLOBALS['extendModule']['modules'])) {
                $distributionOrder = $e->getTarget()->getServiceLocator()->get('DistributionOrderTable')->infoDistributionOrder(array('order_id'=>$orderId, 'o_state'=>2));
                if( $distributionOrder
                    and isset($this->autoOtherConfig['distributionState'])
                    and $this->autoOtherConfig['distributionState'] == 1
                    and isset($this->autoOtherConfig['distributionTime'])
                    and $this->autoOtherConfig['distributionTime'] > 0)
                {
                    $autoOrder = array();
                    $autoOrder['order_id']      = $distributionOrder->order_id;
                    $autoOrder['user_id']       = $distributionOrder->buy_user_id;
                    $autoOrder['user_name']     = $distributionOrder->buy_user_name;
                    $autoOrder['change_time']   = time() + $this->autoOtherConfig['distributionTime'];

                    $e->getTarget()->getServiceLocator()->get('DbAutoDistributionOrderTable')->addAutoDistributionOrder($autoOrder);
                }
            }
        }
    }

    /**
     * 订单完成时(前台+后台)，对于会员优惠券的有效修改
     * @param Event $e
     */
    public function onUpdateUserCoupon(Event $e)
    {
        $orderId = $e->getParam('values');
        if(is_numeric($orderId) and $orderId > 0) {
            $userCouponTable = $e->getTarget()->getServiceLocator()->get('UserCouponTable');
            $orderTable = $e->getTarget()->getServiceLocator()->get('OrderTable');

            $orderInfo = $orderTable->infoOrder(array('order_id'=>$orderId));
            if($orderInfo->buyer_id > 0) {
                $userCouponArray = $userCouponTable->listUserCoupon(array('coupon_use_state'=>'0', 'get_order_id'=>$orderId, 'user_id'=>$orderInfo->buyer_id));
                if(is_array($userCouponArray) and !empty($userCouponArray)) {
                    foreach($userCouponArray as $couponValue) {
                        if($couponValue['coupon_use_state'] == 0) {
                            $userCouponTable->updateUserCoupon(array('coupon_use_state'=>1), array('user_coupon_id'=>$couponValue['user_coupon_id'], 'get_order_id'=>$orderInfo->order_id, 'user_id'=>$orderInfo->buyer_id));
                        }
                    }
                }
            }
        }
    }
}