<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbauto\Controller;

use Admin\Controller\BaseController;
use Zend\Config\Reader\Ini;

class IndexController extends BaseController
{
    /**
     * 自动处理基本信息
     */
    public function indexAction ()
    {
        $array      = array();
        $array['page_type'] = 'index';//页面类型，用于标识页面左侧的菜单
        $iniRead    = new Ini();
        $confPath   = DBSHOP_PATH . '/module/Extendapp/Dbauto/data/config.ini';

        if($this->request->isPost()) {
            $configWArray   = array();
            $postArray      = $this->request->getPost()->toArray();

            $configWArray['autotype']         = isset($postArray['autotype'])           ? $postArray['autotype']        : '';
            $configWArray['dbshopAutoState']  = isset($postArray['dbshopAutoState'])    ? $postArray['dbshopAutoState'] : '';

            if(is_array($postArray['dbshopauto'][$postArray['autotype']]) and !empty($postArray['dbshopauto'][$postArray['autotype']])) {
                foreach($postArray['dbshopauto'][$postArray['autotype']] as $key => $value) {
                    $configWArray[$postArray['autotype']][$key] = $value;
                }
            }
            $iniWrite = new \Zend\Config\Writer\Ini();
            $iniWrite->toFile($confPath, $configWArray);

            $array['success_msg'] = $this->getDbshopLang()->translate('自动处理基本信息设置成功！');
        }

        if(file_exists($confPath)) {
            $array['dbauto'] = $iniRead->fromFile($confPath);
        }

        return $array;
    }
    /**
     * 订单自动处理设置
     * @return mixed
     */
    public function orderAutoAction()
    {
        $array = array();
        $array['page_type'] = 'order';//页面类型，用于标识页面左侧的菜单

        $iniRead    = new Ini();
        $confPath   = DBSHOP_PATH . '/module/Extendapp/Dbauto/data/auto/order.ini';
        if($this->request->isPost()) {
            $autoOrderArray = array();
            $postArray      = $this->request->getPost()->toArray();
            //自动取消订单
            $autoOrderArray['cancel'] = array(
                'time'    => $postArray['cancelTime'],
                'state'   => isset($postArray['cancelState']) ? $postArray['cancelState'] : ''
            );
            //自动确认收货
            $autoOrderArray['receipt'] = array(
                'time'   => $postArray['receiptTime'],
                'state'  => isset($postArray['receiptState']) ? $postArray['receiptState'] : ''
            );
            //自动评价订单
            $autoOrderArray['evaluate'] = array(
                'time'   => $postArray['evaluateTime'],
                'state'  => isset($postArray['evaluateState']) ? $postArray['evaluateState'] : ''
            );

            $iniWrite = new \Zend\Config\Writer\Ini();
            $iniWrite->toFile($confPath, $autoOrderArray);

            $array['success_msg'] = $this->getDbshopLang()->translate('订单处理设置成功！');
        }

        if(file_exists($confPath)) {
            $array['order'] = $iniRead->fromFile($confPath);
        }

        return $array;
    }
    public function otherAutoAction()
    {
        $array = array();
        $array['page_type'] = 'other';//页面类型，用于标识页面左侧的菜单

        $iniRead    = new Ini();
        $confPath   = DBSHOP_PATH . '/module/Extendapp/Dbauto/data/auto/otherauto.ini';
        if($this->request->isPost()) {
            $otherAutoArray = array();
            $postArray      = $this->request->getPost()->toArray();
            //优惠券过期自动处理
            $otherAutoArray['couponOper'] = isset($postArray['couponOper']) ? $postArray['couponOper'] : '';

            $otherAutoArray['distributionTime'] = isset($postArray['distributionTime']) ? $postArray['distributionTime'] : '';
            $otherAutoArray['distributionState'] = isset($postArray['distributionState']) ? $postArray['distributionState'] : '';

            $iniWrite = new \Zend\Config\Writer\Ini();
            $iniWrite->toFile($confPath, $otherAutoArray);

            $array['success_msg'] = $this->getDbshopLang()->translate('自动处理设置成功！');
        }

        if(file_exists($confPath)) {
            $array['otherauto'] = $iniRead->fromFile($confPath);
        }

        return $array;
    }
    /**
     * 生成调用密钥
     */
    public function ajaxCreateKeyAction()
    {
        $type       = 'dbshopAuto';
        $chars      = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $str        = $type . $chars[rand(0, 25)] . str_replace('.', '',microtime(true));
        $apiKey     = md5($str);
        exit($apiKey);
    }
}
