<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbauto\Controller;

use Zend\Config\Reader\Ini;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AutoController extends AbstractActionController
{
    private $dbTables = array();
    private $translator;

    /**
     * 默认自动执行
     * @return ViewModel
     */
    public function indexAction()
    {
        $autoKey = $this->request->getQuery('key');
        $iniRead = new Ini();
        $autoConfig = $iniRead->fromFile(DBSHOP_PATH . '/module/Extendapp/Dbauto/data/config.ini');
        //开启，并且key匹配的情况下，进行自动处理操作
        $autoType = $autoConfig['autotype'];
        if($autoConfig['dbshopAutoState'] == 1 and !empty($autoKey) and $autoConfig[$autoType]['key'] == $autoKey) {
            $time = time();//通用时间点

            //对优惠券的自动处理
            $this->changeCouponState($time, $iniRead);

            //对分销提成进行处理
            $this->distributionOper($time, $iniRead);

            //对订单的自动处理
            $autoOrderArray = $this->getDbshopTable('DbautoOrderTable')->listAutoOrder(array('change_time <= '.$time));
            if(is_array($autoOrderArray) and !empty($autoOrderArray)) {
                foreach($autoOrderArray as $autoValue) {
                    $orderInfo = $this->getDbshopTable('OrderTable')->infoOrder(array('order_id'=>$autoValue['order_id']));

                    if(isset($orderInfo->order_id) and $orderInfo->order_id > 0) {
                        switch($autoValue['order_change_state']) {
                            case 0://取消订单
                                if($orderInfo->order_state == $autoValue['order_state']) {
                                    $this->cancelOrder($autoValue, $orderInfo);
                                }
                                break;

                            case 60://确认收货订单
                                if($orderInfo->order_state == $autoValue['order_state']) {
                                    $this->receiptOrder($autoValue, $orderInfo);
                                }
                                break;

                            case 100://自动评价(如果该订单在退货里存在，不管是不是部分商品退货，都不进行自动评价)
                                if($orderInfo->order_state == $autoValue['order_state']) {
                                    $this->commentOrder($autoValue, $orderInfo);
                                }
                                break;
                        }
                    }
                    $this->getDbshopTable('DbautoOrderTable')->delAutoOrder(array('order_auto_id'=>$autoValue['order_auto_id']));
                }
            }
        }

        $view = new ViewModel();
        $view->setTerminal(true);

        $array = array();

        return $view->setVariables(array());
    }
    /**
     * 自动处理过期优惠券
     * @param $time
     * @param $ini
     */
    private function changeCouponState($time, $ini)
    {
        $configPath = DBSHOP_PATH . '/module/Extendapp/Dbauto/data/auto/otherauto.ini';
        if(!file_exists($configPath)) return;

        $autoConfig = $ini->fromFile($configPath);
        if(isset($autoConfig['couponOper']) and $autoConfig['couponOper'] == '1') {
            $couponArray = $this->getDbshopTable('UserCouponTable')->listUserCoupon(array('coupon_expire_time IS NOT NULL and coupon_use_state IN (0,1) and coupon_expire_time<='.$time));
            $userCouponIdArray = array();
            if(is_array($couponArray) and !empty($couponArray)) {
                foreach($couponArray as $value) {
                    if(!empty($value['coupon_expire_time']) and $time >= $value['coupon_expire_time']) $userCouponIdArray[] = $value['user_coupon_id'];
                }
            }
            if(!empty($userCouponIdArray)) $this->getDbshopTable('UserCouponTable')->updateUserCoupon(array('coupon_use_state'=>3), array('user_coupon_id IN ('.implode(',', $userCouponIdArray).')'));
        }
    }

    private function distributionOper($time, $iniRead)
    {
        $configPath = DBSHOP_PATH . '/module/Extendapp/Dbauto/data/auto/otherauto.ini';
        if (!file_exists($configPath)) return;

        if (isset($GLOBALS['extendModule']['modules']) and !empty($GLOBALS['extendModule']['modules']) and in_array('Dbdistribution', $GLOBALS['extendModule']['modules'])) {
            $autoConfig = $iniRead->fromFile($configPath);
            if (
                isset($autoConfig['distributionState'])
                and $autoConfig['distributionState'] == 1
                and isset($autoConfig['distributionTime'])
                and $autoConfig['distributionTime'] > 0) {
                $distributionOrder = $this->getDbshopTable('DbAutoDistributionOrderTable')->listAutoDistributionOrder(array('change_time<=' . $time . ' and o.o_state=2'));
                if ($distributionOrder) {
                    foreach ($distributionOrder as $orderValue) {
                        $userInfo = $this->getDbshopTable('UserTable')->infoUser(array('user_id' => $orderValue['one_level_user_id']));
                        if ($userInfo) {
                            $moneyLogArray = array();
                            $moneyLogArray['user_id'] = $orderValue['one_level_user_id'];
                            $moneyLogArray['user_name'] = $orderValue['one_level_user_name'];
                            $moneyLogArray['money_change_num'] = $orderValue['one_level_user_cost'];
                            $moneyLogArray['money_pay_state'] = 20;//20是已经处理
                            $moneyLogArray['money_pay_type'] = 5;//支付类型，分销提成
                            $moneyLogArray['money_changed_amount'] = $userInfo->user_money + $moneyLogArray['money_change_num'];
                            $moneyLogArray['money_pay_info'] = '分销提成，提成订单号' . $orderValue['order_sn'];

                            $state = $this->getDbshopTable('UserMoneyLogTable')->addUserMoneyLog($moneyLogArray);

                            if ($state) {
                                $this->getDbshopTable('UserTable')->updateUser(array('user_money' => $moneyLogArray['money_changed_amount']), array('user_id' => $userInfo->user_id));
                            }
                        }

                        if ($orderValue['top_level_user_id'] > 0) {
                            $userInfo = $this->getDbshopTable('UserTable')->infoUser(array('user_id' => $orderValue['top_level_user_id']));
                            if ($userInfo) {
                                $moneyLogArray = array();
                                $moneyLogArray['user_id'] = $orderValue['top_level_user_id'];
                                $moneyLogArray['user_name'] = $orderValue['top_level_user_name'];
                                $moneyLogArray['money_change_num'] = $orderValue['top_level_user_cost'];
                                $moneyLogArray['money_pay_state'] = 20;//20是已经处理
                                $moneyLogArray['money_pay_type'] = 5;//分销提成
                                $moneyLogArray['money_changed_amount'] = $userInfo->user_money + $moneyLogArray['money_change_num'];
                                $moneyLogArray['money_pay_info'] = '分销提成，提成订单号' . $orderValue['order_sn'];

                                $state = $this->getDbshopTable('UserMoneyLogTable')->addUserMoneyLog($moneyLogArray);

                                if ($state) {
                                    $this->getDbshopTable('UserTable')->updateUser(array('user_money' => $moneyLogArray['money_changed_amount']), array('user_id' => $userInfo->user_id));
                                }
                            }
                        }
                        $this->getDbshopTable('DistributionOrderTable')->updateDistributionOrder(array('o_state'=>1, 'o_finish_time'=>time()), array('order_id'=>$orderValue['order_id'], 'o_state'=>2));
                    }
                }
            }
        }
    }

    /**
     * 订单评价
     * @param $autoValue
     * @param $orderInfo
     */
    private function commentOrder($autoValue, $orderInfo)
    {
        $refundInfo = $this->getDbshopTable('OrderRefundTable')->infoOrderRefund(array('order_sn'=>$orderInfo->order_sn));
        if(!$refundInfo) {
            $orderGoodsArray = $this->getDbshopTable('OrderGoodsTable')->listOrderGoods(array('order_id'=>$autoValue['order_id']));
            if(is_array($orderGoodsArray) and !empty($orderGoodsArray)) {
                foreach($orderGoodsArray as $goodsValue){
                    $commentArray['comment_body']   = $this->getDbshopLang()->translate('该商品非常好，值得购买！');
                    $commentArray['goods_evaluation']= 5;
                    $commentArray['comment_writer'] = $autoValue['user_name'];
                    $commentArray['comment_time']   = time();
                    $commentArray['goods_id']       = $goodsValue['goods_id'];
                    $commentArray['order_goods_id'] = $goodsValue['order_goods_id'];

                    if($this->getDbshopTable('GoodsCommentTable')->addGoodsComment($commentArray)) {
                        $listCommentArray = $this->getDbshopTable('GoodsCommentTable')->allGoodsComment(array('goods_id'=>$goodsValue['goods_id']));
                        if(is_array($listCommentArray) and !empty($listCommentArray)) $commentCount = count($listCommentArray); else $commentCount = 1;

                        //添加订单商品基础评价
                        $baseCommentArray = array();
                        $baseCommentArray['goods_id']       = $goodsValue['goods_id'];
                        $baseCommentArray['comment_writer'] = $commentArray['comment_writer'];
                        $baseCommentArray['comment_time']   = $commentArray['comment_time'];
                        $baseCommentArray['comment_count']  = $commentCount;
                        $baseCommentInfo  = $this->getDbshopTable('GoodsCommentBaseTable')->InfoGoodsCommentBase(array('goods_id'=>$goodsValue['goods_id']));
                        if($baseCommentInfo) {
                            $this->getDbshopTable('GoodsCommentBaseTable')->updataGoodsCommentBase($baseCommentArray, array('goods_id'=>$goodsValue['goods_id']));
                        } else {
                            $this->getDbshopTable('GoodsCommentBaseTable')->addGoodsCommentBase($baseCommentArray);
                        }
                        //更新订单商品评价状态
                        $this->getDbshopTable('OrderGoodsTable')->updateOrderGoods(array('comment_state'=>1), array('order_goods_id'=>$goodsValue['order_goods_id'], 'buyer_id'=>$autoValue['user_id']));
                        //更新订单表中的序列化商品评价状态
                        $orderGoodsSerArray = unserialize($orderInfo->goods_serialize);
                        $orderGoodsSerArray[$goodsValue['order_goods_id']]['comment_state'] = 1;
                        $this->getDbshopTable('OrderTable')->updateOrder(array('goods_serialize'=>serialize($orderGoodsSerArray)), array('order_id'=>$orderInfo->order_id));
                    }
                }
            }
        }
    }
    /**
     * 确认收货操作
     * @param $autoValue
     * @param $orderInfo
     */
    private function receiptOrder($autoValue, $orderInfo)
    {
        $finishTime = time();
        //如果未付款，则修改为已付款状态
        if(empty($orderInfo->pay_time)) {
            $this->getDbshopTable('OrderTable')->updateOrder(array('pay_time'=>$finishTime), array('order_id'=>$autoValue['order_id']));
            //保存订单历史
            $this->getDbshopTable('OrderLogTable')->addOrderLog(array('order_id'=>$autoValue['order_id'], 'order_state'=>20, 'state_info'=>$this->getDbshopLang()->translate('系统自动确认收款'), 'log_time'=>$finishTime, 'log_user'=>$this->getDbshopLang()->translate('系统')));
        }
        //确认收货
        $this->getDbshopTable('OrderTable')->updateOrder(array('order_state'=>60, 'finish_time'=>$finishTime), array('order_id'=>$autoValue['order_id']));
        //事件驱动
        $this->getEventManager()->trigger('order.finish.backstage.post', $this, array('values'=>$orderInfo->order_id));
        //保存订单历史
        $this->getDbshopTable('OrderLogTable')->addOrderLog(array('order_id'=>$autoValue['order_id'], 'order_state'=>60, 'state_info'=>$this->getDbshopLang()->translate('系统自动确认收货'), 'log_time'=>$finishTime, 'log_user'=>$this->getDbshopLang()->translate('系统')));
        //积分获取
        $array['order_info'] = $orderInfo;
        if($array['order_info']->integral_num > 0 or $array['order_info']->integral_type_2_num > 0) {
            $integralLogArray = array();
            $integralLogArray['user_id']           = $array['order_info']->buyer_id;
            $integralLogArray['user_name']         = $array['order_info']->buyer_name;
            $integralLogArray['integral_log_info'] = $this->getDbshopLang()->translate('商品购物，订单号为：') . $array['order_info']->order_sn . '<br>';
            $integralLogArray['integral_log_time'] = time();

            if($array['order_info']->integral_num > 0) {//消费积分
                $integralLogArray['integral_num_log']  = $array['order_info']->integral_num;
                $integralLogArray['integral_log_info'] .= $array['order_info']->integral_rule_info;
                if($this->getDbshopTable('IntegralLogTable')->addIntegralLog($integralLogArray)) {
                    //会员消费积分更新
                    $this->getDbshopTable('UserTable')->updateUserIntegralNum($integralLogArray, array('user_id'=>$array['order_info']->buyer_id));
                }
            }
            if($array['order_info']->integral_type_2_num > 0) {//等级积分
                $integralLogArray['integral_num_log']  = $array['order_info']->integral_type_2_num;
                $integralLogArray['integral_log_info'] .= $array['order_info']->integral_type_2_num_rule_info;
                $integralLogArray['integral_type_id'] = 2;
                if($this->getDbshopTable('IntegralLogTable')->addIntegralLog($integralLogArray)) {
                    //会员等级积分更新
                    $this->getDbshopTable('UserTable')->updateUserIntegralNum($integralLogArray, array('user_id'=>$array['order_info']->buyer_id), 2);
                    //等级积分变更后，判断等级是否有变化
                    $userInfo = $this->getDbshopTable('UserTable')->infoUser(array('user_id'=>$array['order_info']->buyer_id));
                    $groupId = $this->getDbshopTable('UserGroupTable')->checkUserGroup(array('group_id'=>$userInfo->group_id, 'integral_num'=>$userInfo->integral_type_2_num));
                    if($groupId) {
                        $this->getDbshopTable('UserTable')->updateUser(array('group_id'=>$groupId), array('user_id'=>$userInfo->user_id));
                        $userGroup = $this->getDbshopTable('UserGroupExtendTable')->infoUserGroupExtend(array('group_id'=>$groupId,'language'=>$this->getDbshopLang()->getLocale()));
                        $this->getServiceLocator()->get('frontHelper')->setUserSession(array('group_id'=>$groupId));
                        $this->getServiceLocator()->get('frontHelper')->setUserSession(array('user_group_name'=>$userGroup->group_name));
                    }
                }
            }
        }
        /*----------------------提醒信息发送----------------------*/
        //订单配送信息
        $deliveryAddress = $this->getDbshopTable('OrderDeliveryAddressTable')->infoDeliveryAddress(array('order_id'=>$autoValue['order_id']));

        $sendArray['buyer_id']    = $orderInfo->buyer_id;
        $sendArray['buyer_name']  = $array['order_info']->buyer_name;
        $sendArray['order_sn']    = $array['order_info']->order_sn;
        $sendArray['order_total'] = $array['order_info']->order_amount;
        $sendArray['express_name']  = $deliveryAddress->express_name;
        $sendArray['express_number']= $deliveryAddress->express_number;
        $sendArray['time']        = $finishTime;
        $sendArray['buyer_email'] = $array['order_info']->buyer_email;
        $sendArray['order_state'] = 'transaction_finish';
        $sendArray['time_type']   = 'finishtime';
        $sendArray['subject']     = $this->getDbshopLang()->translate('确认收货交易完成');
        $this->changeStateSendMail($sendArray);
        /*----------------------提醒信息发送----------------------*/

        /*----------------------手机提醒信息发送----------------------*/
        $smsData = array(
            'buyname'   => $sendArray['buyer_name'],
            'ordersn'    => $sendArray['order_sn'],
            'ordertotal' => $sendArray['order_total'],
            'expressname'=> $sendArray['express_name'],
            'expressnumber' => $sendArray['express_number'],
            'time'    => $sendArray['time'],
        );
        try {
            $this->getServiceLocator()->get('shop_send_sms')->toSendSms(
                $smsData,
                $autoValue['user_phone'],
                'alidayu_finish_order_template_id',
                $autoValue['user_id']
            );
        } catch(\Exception $e) {

        }
        /*----------------------手机提醒信息发送----------------------*/
    }
    /**
     * 取消订单
     * @param $autoValue
     * @param $orderInfo
     */
    private function cancelOrder($autoValue, $orderInfo)
    {
        $this->getDbshopTable('OrderTable')->updateOrder(array('order_state'=>0), array('order_id'=>$autoValue['order_id']));
        $this->returnGoodsStock($autoValue['order_id']);//取消订单时的库存处理
        //加入状态记录
        $this->getDbshopTable('OrderLogTable')->addOrderLog(array('order_id'=>$autoValue['order_id'], 'order_state'=>'0', 'state_info'=>$this->getDbshopLang()->translate('系统自动取消'), 'log_time'=>time(), 'log_user'=>$this->getDbshopLang()->translate('系统')));
        //检查是否有消费积分付款
        if($orderInfo->integral_buy_num > 0) {
            $integralLogArray = array();
            $integralLogArray['user_id']           = $orderInfo->buyer_id;
            $integralLogArray['user_name']         = $orderInfo->buyer_name;
            $integralLogArray['integral_log_info'] = $this->getDbshopLang()->translate('取消订单，订单号为：') . $orderInfo->order_sn;
            $integralLogArray['integral_num_log']  = $orderInfo->integral_buy_num;
            $integralLogArray['integral_log_time'] = time();
            if($this->getDbshopTable('IntegralLogTable')->addIntegralLog($integralLogArray)) {
                //会员消费积分更新
                $this->getDbshopTable('UserTable')->updateUserIntegralNum($integralLogArray, array('user_id'=>$integralLogArray['user_id']));
            }
        }

        /*----------------------提醒信息发送----------------------*/
        $sendArray['buyer_id']    = $orderInfo->buyer_id;
        $sendArray['buyer_name']  = $orderInfo->buyer_name;
        $sendArray['order_sn']    = $orderInfo->order_sn;
        $sendArray['order_total'] = $orderInfo->order_amount;
        $sendArray['cancelinfo'] = $this->getDbshopLang()->translate('系统自动取消');
        $sendArray['time']        = time();
        $sendArray['buyer_email'] = $orderInfo->buyer_email;
        $sendArray['order_state'] = 'cancel_order';
        $sendArray['time_type']   = 'canceltime';
        $sendArray['subject']     = $this->getDbshopLang()->translate('订单取消');
        $this->changeStateSendMail($sendArray);
        /*----------------------提醒信息发送----------------------*/
        /*----------------------手机提醒信息发送----------------------*/
        $smsData = array(
            'buyname'   => $sendArray['buyer_name'],
            'ordersn'   => $sendArray['order_sn'],
            'ordertotal'=> $sendArray['order_total'],
            'time'    => $sendArray['time'],
        );
        try {
            $this->getServiceLocator()->get('shop_send_sms')->toSendSms(
                $smsData,
                $autoValue['user_phone'],
                'alidayu_cancel_order_template_id',
                $autoValue['user_id']
            );
        } catch(\Exception $e) {

        }
        /*----------------------手机提醒信息发送----------------------*/

        $this->getEventManager()->trigger('order.cancel.front.post', $this, array('values'=>$autoValue['order_id']));
    }
    /**
     * 订单变更发送邮件
     * @param array $data
     */
    private function changeStateSendMail(array $data)
    {
        $sendMessageBody = $this->getServiceLocator()->get('frontHelper')->getSendMessageBody($data['order_state']);
        if($sendMessageBody != '') {
            $sendArray = array();
            $sendArray['shopname']      = $this->getServiceLocator()->get('frontHelper')->websiteInfo('shop_name');
            $sendArray['buyname']       = $data['buyer_name'];
            $sendArray['ordersn']       = $data['order_sn'];
            $sendArray['ordertotal']    = isset($data['order_total'])    ? $data['order_total'] : '';
            $sendArray['expressname']   = isset($data['express_name'])   ? $data['express_name'] : '';
            $sendArray['expressnumber'] = isset($data['express_number']) ? $data['express_number'] : '';
            $sendArray['cancel_info']   = isset($data['cancelinfo']) ? $data['cancelinfo'] : '';
            $sendArray[$data['time_type']]= $data['time'];
            $sendArray['shopurl']       = $this->getServiceLocator()->get('frontHelper')->dbshopHttpOrHttps() . $this->getServiceLocator()->get('frontHelper')->dbshopHttpHost() . $this->url()->fromRoute('shopfront/default');

            $sendArray['subject']       = $sendArray['shopname'] . $data['subject'];
            $sendArray['send_mail'][]   = $this->getServiceLocator()->get('frontHelper')->getSendMessageBuyerEmail($data['order_state'] . '_state', $data['buyer_email']);
            $sendArray['send_mail'][]   = $this->getServiceLocator()->get('frontHelper')->getSendMessageAdminEmail($data['order_state'] . '_state');

            $sendMessageBody            = $this->getServiceLocator()->get('frontHelper')->createSendMessageContent($sendArray, $sendMessageBody);
            try {
                $sendState = $this->getServiceLocator()->get('shop_send_mail')->SendMesssageMail($sendArray, $sendMessageBody);
                $sendState = ($sendState ? 1 : 2);
            } catch (\Exception $e) {
                $sendState = 2;
            }
            //记录给用户发的电邮
            if($sendArray['send_mail'][0] != '') {
                $sendLog = array(
                    'mail_subject' => $sendArray['subject'],
                    'mail_body'    => $sendMessageBody,
                    'send_time'    => time(),
                    'user_id'      => $data['buyer_id'],
                    'send_state'   => $sendState
                );
                $this->getDbshopTable('UserMailLogTable')->addUserMailLog($sendLog);
            }
        }
    }
    /**
     * 取消订单时的库存处理
     * @param unknown $orderId
     */
    private function returnGoodsStock ($orderId)
    {
        $goodsArray = $this->getDbshopTable('OrderGoodsTable')->listOrderGoods(array('order_id'=>$orderId));
        if(is_array($goodsArray) and !empty($goodsArray)) {
            foreach ($goodsArray as $goodsValue) {
                $goodsInfo = '';
                $goodsInfo = $this->getDbshopTable('GoodsTable')->oneGoodsInfo(array('goods_id'=>$goodsValue['goods_id']));
                if($goodsInfo->goods_stock_state_open != 1) {//如果没有启用库存状态显示
                    if((!empty($goodsValue['goods_color']) and !empty($goodsValue['goods_size'])) || !empty($goodsValue['goods_spec_tag_id'])) {
                        if(!empty($goodsValue['goods_spec_tag_id'])) $whereExtend = array('goods_id'=>$goodsValue['goods_id'], 'adv_spec_tag_id'=>$goodsValue['goods_spec_tag_id']);
                        else $whereExtend = array('goods_id'=>$goodsValue['goods_id'], 'goods_color'=>$goodsValue['goods_color'], 'goods_size'=>$goodsValue['goods_size']);

                        $extendGoods = $this->getDbshopTable('GoodsPriceExtendGoodsTable')->InfoPriceExtendGoods($whereExtend);
                        $this->getDbshopTable('GoodsPriceExtendGoodsTable')->updatePriceExtendGoods(array('goods_extend_stock'=>($extendGoods->goods_extend_stock + $goodsValue['buy_num'])), $whereExtend);
                    } else {
                        $this->getDbshopTable('GoodsTable')->oneUpdateGoods(array('goods_stock'=>($goodsInfo->goods_stock + $goodsValue['buy_num'])), array('goods_id'=>$goodsValue['goods_id']));
                    }
                }
            }
        }
    }

    /**
     * 数据表调用
     * @param string $tableName
     * @return multitype:
     */
    private function getDbshopTable ($tableName)
    {
        if (empty($this->dbTables[$tableName])) {
            $this->dbTables[$tableName] = $this->getServiceLocator()->get($tableName);
        }
        return $this->dbTables[$tableName];
    }
    /**
     * 语言包调用
     * @return Ambigous <object, multitype:, \Zend\I18n\Translator\Translator>
     */
    private function getDbshopLang ()
    {
        if (! $this->translator) {
            $this->translator = $this->getServiceLocator()->get('translator');
        }
        return $this->translator;
    }
}