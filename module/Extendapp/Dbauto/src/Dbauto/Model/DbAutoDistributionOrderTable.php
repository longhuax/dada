<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbauto\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Dbauto\Model\DbAutoDistributionOrder as dbshopCheckInData;

class DbAutoDistributionOrderTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_dbauto_distribution_order';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter     = $adapter;
        $this->initialize();
    }

    /**
     * 添加分销自动处理操作
     * @param array $data
     * @return int|null
     */
    public function addAutoDistributionOrder(array $data)
    {
        $row = $this->insert(dbshopCheckInData::addAutoDistributionOrderData($data));
        if($row) {
            return $this->getLastInsertValue();
        }
        return null;
    }

    /**
     * 分销的自动处理列表
     * @param array $where
     * @return array|null
     */
    public function listAutoDistributionOrder(array $where)
    {
        $result = $this->select(function (Select $select) use ($where) {
            $select->join(array('o'=>'dbshop_distribution_order'), 'o.order_id=dbshop_dbauto_distribution_order.order_id',
                array(
                    'order_sn',
                    'one_level_user_id',
                    'one_level_user_name',
                    'one_level_user_cost',
                    'top_level_user_id',
                    'top_level_user_name',
                    'top_level_user_cost'
                    )
            );
            $select->where($where);
        });
        if($result) {
            return $result->toArray();
        }
        return null;
    }
}