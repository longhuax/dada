<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbauto\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\AbstractTableGateway;
use \Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Dbauto\Model\DbautoOrder as dbshopCheckInData;

class DbautoOrderTable extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'dbshop_dbauto_order';

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter     = $adapter;
        $this->initialize();
    }
    /**
     * 添加订单的自动处理信息
     * @param array $data
     * @return int|null
     */
    public function addAutoOrder(array $data)
    {
        $row = $this->insert(dbshopCheckInData::addAutoOrderData($data));
        if($row) {
            return $this->getLastInsertValue();
        }
        return null;
    }
    /**
     * 获取自动处理的信息
     * @param array $where
     * @return array|null
     */
    public function listAutoOrder(array $where=array())
    {
        $result = $this->select(function(Select $select) use ($where) {
           $select->columns(array("*", new Expression('
                (SELECT u.user_phone FROM dbshop_user AS u WHERE u.user_id=dbshop_dbauto_order.user_id) AS user_phone
           ')));
            $select->where($where);
            $select->order('dbshop_dbauto_order.order_state ASC');
        });

        if($result) {
            return $result->toArray();
        }
        return null;
    }
    /**
     * 删除订单的自动处理信息
     * @param array $where
     * @return int
     */
    public function delAutoOrder(array $where)
    {
        return $this->delete($where);
    }
}