<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2018 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

namespace Dbauto\Model;


class DbAutoDistributionOrder
{
    private static $dataArray = array();

    private static function checkData (array $data)
    {
        self::$dataArray['ao_id'] = (isset($data['ao_id']) and !empty($data['ao_id']))
            ? intval($data['ao_id'])
            : null;

        self::$dataArray['order_id'] = (isset($data['order_id']) and !empty($data['order_id']))
            ? intval($data['order_id'])
            : null;

        self::$dataArray['user_id'] = (isset($data['user_id']) and !empty($data['user_id']))
            ? intval($data['user_id'])
            : null;

        self::$dataArray['user_name'] = (isset($data['user_name']) and !empty($data['user_name']))
            ? trim($data['user_name'])
            : null;

        self::$dataArray['change_time'] = (isset($data['change_time']) and !empty($data['change_time']))
            ? trim($data['change_time'])
            : null;

        self::$dataArray = array_filter(self::$dataArray);

        return self::$dataArray;
    }
    /**
     * 对添加自动处理信息的过滤
     * @param $data
     * @return array
     */
    public static function addAutoDistributionOrderData($data)
    {
        $data = self::checkData($data);
        return $data;
    }
}