DROP TABLE IF EXISTS dbshop_dbauto_order;
CREATE TABLE `dbshop_dbauto_order` (
  `order_auto_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_id` int(11) NOT NULL COMMENT '对应订单id',
  `user_id` int(11) NOT NULL,
  `user_name` char(100) NOT NULL,
  `order_state` int(4) NOT NULL COMMENT '订单状态',
  `order_change_state` int(4) NOT NULL COMMENT '订单将要改变的状态',
  `change_time` int(10) NOT NULL COMMENT '在哪个时间点改变',
  PRIMARY KEY (`order_auto_id`),
  KEY `order_id` (`order_id`,`order_state`,`order_change_state`,`change_time`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS dbshop_dbauto_distribution_order;
CREATE TABLE `dbshop_dbauto_distribution_order` (
  `ao_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` char(100) NOT NULL,
  `change_time` int(10) NOT NULL,
  PRIMARY KEY (`ao_id`),
  KEY `order_id` (`order_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分销提成自动处理';