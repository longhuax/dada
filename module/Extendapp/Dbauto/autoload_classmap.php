<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */
return array(
    'Dbauto\Module'                        => __DIR__ . '/Module.php',
    'Dbauto\Controller\IndexController'    => __DIR__ . '/src/Dbauto/Controller/IndexController.php',
    'Dbauto\Controller\AutoController'     => __DIR__ . '/src/Dbauto/Controller/AutoController.php',
    'Dbauto\Model\DbautoOrder'             => __DIR__ . '/src/Dbauto/Model/DbautoOrder.php',
    'Dbauto\Model\DbautoOrderTable'        => __DIR__ . '/src/Dbauto/Model/DbautoOrderTable.php',
    'Dbauto\Model\DbAutoDistributionOrder' => __DIR__ . '/src/Dbauto/Model/DbAutoDistributionOrder.php',
    'Dbauto\Model\DbAutoDistributionOrderTable' => __DIR__ . '/src/Dbauto/Model/DbAutoDistributionOrderTable.php',

);
