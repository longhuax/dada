<?php
/**
 * DBShop 电子商务系统
 *
 * ==========================================================================
 * @link      http://www.dbshop.net/
 * @copyright Copyright (c) 2012-2017 DBShop.net Inc. (http://www.dbshop.net)
 * @license   http://www.dbshop.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风
 *
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Dbauto\Controller\Index' => 'Dbauto\Controller\IndexController',
            'Dbauto\Controller\Auto' => 'Dbauto\Controller\AutoController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'dbauto'     => include __DIR__ . '/router/router.config.php',
            'frontauto'     => include __DIR__ . '/router/frontauto.config.php',
        ),
    ),
    'translator' => array(
            'translation_file_patterns' => array(
                    array(
                            'type' => 'gettext',
                            'base_dir' => __DIR__ . '/../language',
                            'pattern' => '%s.mo'
                    )
            )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Dbauto' => __DIR__ . '/../view',
        )
    ),
);
