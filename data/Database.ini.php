<?php
return array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=dbshop;port=3306;host=localhost;charset=utf8',
        'username' => 'root',
        'password' => 'sa',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))"
        )
);