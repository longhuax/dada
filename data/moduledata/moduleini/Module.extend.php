<?php
return array(
    'modules' => array(
        0 => 'Dbapi',
        1 => 'Dbdistribution',
        2 => 'Dbauto',
        3 => 'Dbgoodsimport',
        4 => 'Dbpriceandstock',
    ),
);
